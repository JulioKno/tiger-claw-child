<?php
/**
 * The Portfolio template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

$tiger_claw_blog_style = explode('_', tiger_claw_get_theme_option('blog_style'));
$tiger_claw_columns = empty($tiger_claw_blog_style[1]) ? 2 : max(2, $tiger_claw_blog_style[1]);
$tiger_claw_post_format = get_post_format();
$tiger_claw_post_format = empty($tiger_claw_post_format) ? 'standard' : str_replace('post-format-', '', $tiger_claw_post_format);
$tiger_claw_animation = tiger_claw_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_portfolio post_layout_portfolio_'.esc_attr($tiger_claw_columns).' post_format_'.esc_attr($tiger_claw_post_format).(is_sticky() && !is_paged() ? ' sticky' : '') ); ?>
	<?php echo (!tiger_claw_is_off($tiger_claw_animation) ? ' data-animation="'.esc_attr(tiger_claw_get_animation_classes($tiger_claw_animation)).'"' : ''); ?>>
	<?php

	// Sticky label
	if ( is_sticky() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

	$tiger_claw_image_hover = tiger_claw_get_theme_option('image_hover');
	// Featured image
	tiger_claw_show_post_featured(array(
		'thumb_size' => tiger_claw_get_thumb_size(strpos(tiger_claw_get_theme_option('body_style'), 'full')!==false || $tiger_claw_columns < 3 ? 'masonry-big' : 'masonry'),
		'show_no_image' => true,
		'class' => $tiger_claw_image_hover == 'dots' ? 'hover_with_info' : '',
		'post_info' => $tiger_claw_image_hover == 'dots' ? '<div class="post_info">'.esc_html(get_the_title()).'</div>' : ''
	));
	?>
</article>