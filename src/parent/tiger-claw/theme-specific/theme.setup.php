<?php
/**
 * Setup theme-specific fonts and colors
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.22
 */

// Theme init priorities:
// 1 - register filters to add/remove lists items in the Theme Options
// 2 - create Theme Options
// 3 - add/remove Theme Options elements
// 5 - load Theme Options
// 9 - register other filters (for installer, etc.)
//10 - standard Theme init procedures (not ordered)
if ( !function_exists('tiger_claw_customizer_theme_setup1') ) {
	add_action( 'after_setup_theme', 'tiger_claw_customizer_theme_setup1', 1 );
	function tiger_claw_customizer_theme_setup1() {
		
		// -----------------------------------------------------------------
		// -- Theme fonts (Google and/or custom fonts)
		// -----------------------------------------------------------------
		
		// Fonts to load when theme start
		// It can be Google fonts or uploaded fonts, placed in the folder /css/font-face/font-name inside the theme folder
		// Attention! Font's folder must have name equal to the font's name, with spaces replaced on the dash '-'
		// For example: font name 'TeX Gyre Termes', folder 'TeX-Gyre-Termes'
		tiger_claw_storage_set('load_fonts', array(
			// Google font
			array(
				'name'	 => 'Lato',
				'family' => 'sans-serif',
				'styles' => '300,300i,400,400i,700,700i,900,900i'		// Parameter 'style' used only for the Google fonts
				),
			// Font-face packed with theme
			array(
				'name'   => 'Connoisseurs',
				'family' => 'sans-serif'
				)
		));



			// Characters subset for the Google fonts. Available values are: latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese
		tiger_claw_storage_set('load_fonts_subset', 'latin,latin-ext');
		
		// Settings of the main tags
		tiger_claw_storage_set('theme_fonts', array(
			'p' => array(
				'title'				=> esc_html__('Main text', 'tiger-claw'),
				'description'		=> esc_html__('Font settings of the main text of the site', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '1rem',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0.07px',
				'margin-top'		=> '0em',
				'margin-bottom'		=> '1.88em'
				),
			'h1' => array(
				'title'				=> esc_html__('Heading 1', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '3.438em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0px',
				'margin-top'		=> '0.99em',
				'margin-bottom'		=> '0.78em'
				),
			'h2' => array(
				'title'				=> esc_html__('Heading 2', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '2.500em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.37em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.1px',
				'margin-top'		=> '1.44em',
				'margin-bottom'		=> '0.6em'
				),
			'h3' => array(
				'title'				=> esc_html__('Heading 3', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '2.063em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.45em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.15px',
				'margin-top'		=> '1.52em',
				'margin-bottom'		=> '0.59em'
				),
			'h4' => array(
				'title'				=> esc_html__('Heading 4', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '1.500em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.15px',
				'margin-top'		=> '1.68em',
				'margin-bottom'		=> '0.85em'
				),
			'h5' => array(
				'title'				=> esc_html__('Heading 5', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '1.125em',
				'font-weight'		=> '600',
				'font-style'		=> 'normal',
				'line-height'		=> '1.3em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0px',
				'margin-top'		=> '2em',
				'margin-bottom'		=> '1.2em'
				),
			'h6' => array(
				'title'				=> esc_html__('Heading 6', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '1em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0px',
				'margin-top'		=> '1.8em',
				'margin-bottom'		=> '0.94em'
				),
			'logo' => array(
				'title'				=> esc_html__('Logo text', 'tiger-claw'),
				'description'		=> esc_html__('Font settings of the text case of the logo', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '1.8em',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.25em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '1px'
				),
			'button' => array(
				'title'				=> esc_html__('Buttons', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '13px',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '0.8px'
				),
			'input' => array(
				'title'				=> esc_html__('Input fields', 'tiger-claw'),
				'description'		=> esc_html__('Font settings of the input fields, dropdowns and textareas', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '14px',
				'font-weight'		=> '400',
				'font-style'		=> 'italic',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0.8px'
				),
			'info' => array(
				'title'				=> esc_html__('Post meta', 'tiger-claw'),
				'description'		=> esc_html__('Font settings of the post meta: date, counters, share, etc.', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '14px',
				'font-weight'		=> '400',
				'font-style'		=> 'italic',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0.2px',
				'margin-top'		=> '0.4em',
				'margin-bottom'		=> ''
				),
			'menu' => array(
				'title'				=> esc_html__('Main menu', 'tiger-claw'),
				'description'		=> esc_html__('Font settings of the main menu items', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '14px',
				'font-weight'		=> '800',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '0.8px'
				),
			'submenu' => array(
				'title'				=> esc_html__('Dropdown menu', 'tiger-claw'),
				'description'		=> esc_html__('Font settings of the dropdown menu items', 'tiger-claw'),
				'font-family'		=> 'Lato, sans-serif',
				'font-size' 		=> '11px',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '0.9px'
				),
			'other' => array(
				'title'				=> esc_html__('Other', 'tiger-claw'),
				'description'		=> esc_html__('Font settings of the other items', 'tiger-claw'),
				'font-family'		=> 'Connoisseurs, sans-serif'
			)
		));
		
		
		// -----------------------------------------------------------------
		// -- Theme colors for customizer
		// -- Attention! Inner scheme must be last in the array below
		// -----------------------------------------------------------------
		tiger_claw_storage_set('scheme_color_groups', array(
			'main'	=> array(
							'title'			=> esc_html__('Main', 'tiger-claw'),
							'description'	=> esc_html__('Colors of the main content area', 'tiger-claw')
							),
			'alter'	=> array(
							'title'			=> esc_html__('Alter', 'tiger-claw'),
							'description'	=> esc_html__('Colors of the alternative blocks (sidebars, etc.)', 'tiger-claw')
							),
			'extra'	=> array(
							'title'			=> esc_html__('Extra', 'tiger-claw'),
							'description'	=> esc_html__('Colors of the extra blocks (dropdowns, price blocks, table headers, etc.)', 'tiger-claw')
							),
			'inverse' => array(
							'title'			=> esc_html__('Inverse', 'tiger-claw'),
							'description'	=> esc_html__('Colors of the inverse blocks - when link color used as background of the block (dropdowns, blockquotes, etc.)', 'tiger-claw')
							),
			'input'	=> array(
							'title'			=> esc_html__('Input', 'tiger-claw'),
							'description'	=> esc_html__('Colors of the form fields (text field, textarea, select, etc.)', 'tiger-claw')
							),
			)
		);
		tiger_claw_storage_set('scheme_color_names', array(
			'bg_color'	=> array(
							'title'			=> esc_html__('Background color', 'tiger-claw'),
							'description'	=> esc_html__('Background color of this block in the normal state', 'tiger-claw')
							),
			'bg_hover'	=> array(
							'title'			=> esc_html__('Background hover', 'tiger-claw'),
							'description'	=> esc_html__('Background color of this block in the hovered state', 'tiger-claw')
							),
			'bd_color'	=> array(
							'title'			=> esc_html__('Border color', 'tiger-claw'),
							'description'	=> esc_html__('Border color of this block in the normal state', 'tiger-claw')
							),
			'bd_hover'	=>  array(
							'title'			=> esc_html__('Border hover', 'tiger-claw'),
							'description'	=> esc_html__('Border color of this block in the hovered state', 'tiger-claw')
							),
			'text'		=> array(
							'title'			=> esc_html__('Text', 'tiger-claw'),
							'description'	=> esc_html__('Color of the plain text inside this block', 'tiger-claw')
							),
			'text_dark'	=> array(
							'title'			=> esc_html__('Text dark', 'tiger-claw'),
							'description'	=> esc_html__('Color of the dark text (bold, header, etc.) inside this block', 'tiger-claw')
							),
			'text_light'=> array(
							'title'			=> esc_html__('Text light', 'tiger-claw'),
							'description'	=> esc_html__('Color of the light text (post meta, etc.) inside this block', 'tiger-claw')
							),
			'text_link'	=> array(
							'title'			=> esc_html__('Link', 'tiger-claw'),
							'description'	=> esc_html__('Color of the links inside this block', 'tiger-claw')
							),
			'text_hover'=> array(
							'title'			=> esc_html__('Link hover', 'tiger-claw'),
							'description'	=> esc_html__('Color of the hovered state of links inside this block', 'tiger-claw')
							),
			'text_link2'=> array(
							'title'			=> esc_html__('Link 2', 'tiger-claw'),
							'description'	=> esc_html__('Color of the accented texts (areas) inside this block', 'tiger-claw')
							),
			'text_hover2'=> array(
							'title'			=> esc_html__('Link 2 hover', 'tiger-claw'),
							'description'	=> esc_html__('Color of the hovered state of accented texts (areas) inside this block', 'tiger-claw')
							),
			'text_link3'=> array(
							'title'			=> esc_html__('Link 3', 'tiger-claw'),
							'description'	=> esc_html__('Color of the other accented texts (buttons) inside this block', 'tiger-claw')
							),
			'text_hover3'=> array(
							'title'			=> esc_html__('Link 3 hover', 'tiger-claw'),
							'description'	=> esc_html__('Color of the hovered state of other accented texts (buttons) inside this block', 'tiger-claw')
							)
			)
		);
		tiger_claw_storage_set('schemes', array(
		
			// Color scheme: 'default'
			'default' => array(
				'title'	 => esc_html__('Default', 'tiger-claw'),
				'colors' => array(
					
					// Whole block border and background
					'bg_color'			=> '#f7f7f7', //
					'bd_color'			=> '#e5e5e5',
		
					// Text and links colors
					'text'				=> '#838487', //
					'text_light'		=> '#999999', //
					'text_dark'			=> '#212226', //
					'text_link'			=> '#ff5001', //
					'text_hover'		=> '#212226', //
					'text_link2'		=> '#ff5001', //
					'text_hover2'		=> '#212226', //
					'text_link3'		=> '#ddb837',
					'text_hover3'		=> '#eec432',
		
					// Alternative blocks (sidebar, tabs, alternative blocks, etc.)
					'alter_bg_color'	=> '#ffffff', //
					'alter_bg_hover'	=> '#e6e8eb',
					'alter_bd_color'	=> '#e5e5e5',
					'alter_bd_hover'	=> '#dadada',
					'alter_text'		=> '#838487', //
					'alter_light'		=> '#b2b2b2', //
					'alter_dark'		=> '#1d1d1d',
					'alter_link'		=> '#ff5001', //
					'alter_hover'		=> '#212226', //
					'alter_link2'		=> '#80d572',
					'alter_hover2'		=> '#8be77c',
					'alter_link3'		=> '#ddb837',
					'alter_hover3'		=> '#eec432',
		
					// Extra blocks (submenu, tabs, color blocks, etc.)
					'extra_bg_color'	=> '#1e1d22',
					'extra_bg_hover'	=> '#28272e',
					'extra_bd_color'	=> '#313131',
					'extra_bd_hover'	=> '#3d3d3d',
					'extra_text'		=> '#bfbfbf',
					'extra_light'		=> '#afafaf',
					'extra_dark'		=> '#ffffff',
					'extra_link'		=> '#72cfd5',
					'extra_hover'		=> '#fe7259',
					'extra_link2'		=> '#80d572',
					'extra_hover2'		=> '#8be77c',
					'extra_link3'		=> '#ddb837',
					'extra_hover3'		=> '#eec432',
		
					// Input fields (form's fields and textarea)
					'input_bg_color'	=> '#f2f2f2', //
					'input_bg_hover'	=> '#f7f7f7', //
					'input_bd_color'	=> '#e1e1e1', //
					'input_bd_hover'	=> '#ff5001', //
					'input_text'		=> '#838487', //
					'input_light'		=> '#e1e1e1', //
					'input_dark'		=> '#171717', //
					
					// Inverse blocks (text and links on the 'text_link' background)
					'inverse_bd_color'	=> '#67bcc1',
					'inverse_bd_hover'	=> '#5aa4a9',
					'inverse_text'		=> '#1d1d1d',
					'inverse_light'		=> '#333333',
					'inverse_dark'		=> '#212226', //
					'inverse_link'		=> '#ffffff',
					'inverse_hover'		=> '#ffffff' //
				)
			),
		
			// Color scheme: 'dark'
			'dark' => array(
				'title'  => esc_html__('Dark', 'tiger-claw'),
				'colors' => array(
					
					// Whole block border and background
					'bg_color'			=> '#212226', //
					'bd_color'			=> '#1c1b1f',
		
					// Text and links colors
					'text'				=> '#b7b7b7',
					'text_light'		=> '#5f5f5f',
					'text_dark'			=> '#ffffff',
					'text_link'			=> '#ff5001', //
					'text_hover'		=> '#ffffff',
					'text_link2'		=> '#ff5001', //
					'text_hover2'		=> '#ffffff', //
					'text_link3'		=> '#ddb837',
					'text_hover3'		=> '#eec432',

					// Alternative blocks (sidebar, tabs, alternative blocks, etc.)
					'alter_bg_color'	=> '#1e1d22', //
					'alter_bg_hover'	=> '#28272e',
					'alter_bd_color'	=> '#313131',
					'alter_bd_hover'	=> '#3d3d3d',
					'alter_text'		=> '#838487', //
					'alter_light'		=> '#5f5f5f',
					'alter_dark'		=> '#ffffff',
					'alter_link'		=> '#ff5001', //
					'alter_hover'		=> '#ffffff',
					'alter_link2'		=> '#80d572',
					'alter_hover2'		=> '#8be77c',
					'alter_link3'		=> '#ddb837',
					'alter_hover3'		=> '#eec432',

					// Extra blocks (submenu, tabs, color blocks, etc.)
					'extra_bg_color'	=> '#1e1d22',
					'extra_bg_hover'	=> '#28272e',
					'extra_bd_color'	=> '#313131',
					'extra_bd_hover'	=> '#3d3d3d',
					'extra_text'		=> '#a6a6a6',
					'extra_light'		=> '#5f5f5f',
					'extra_dark'		=> '#ffffff',
					'extra_link'		=> '#ffaa5f',
					'extra_hover'		=> '#fe7259',
					'extra_link2'		=> '#80d572',
					'extra_hover2'		=> '#8be77c',
					'extra_link3'		=> '#ddb837',
					'extra_hover3'		=> '#eec432',

					// Input fields (form's fields and textarea)
					'input_bg_color'	=> '#f2f2f2', //
					'input_bg_hover'	=> '#2e2d32',
					'input_bd_color'	=> '#2e2d32',
					'input_bd_hover'	=> '#ff5001', //
					'input_text'		=> '#b7b7b7',
					'input_light'		=> '#5f5f5f',
					'input_dark'		=> '#ffffff',
					
					// Inverse blocks (text and links on the 'text_link' background)
					'inverse_bd_color'	=> '#e36650',
					'inverse_bd_hover'	=> '#cb5b47',
					'inverse_text'		=> '#1d1d1d',
					'inverse_light'		=> '#5f5f5f',
					'inverse_dark'		=> '#212226', //
					'inverse_link'		=> '#ffffff',
					'inverse_hover'		=> '#212226' //
				)
			)
		
		));
	}
}

			
// Additional (calculated) theme-specific colors
// Attention! Don't forget setup custom colors also in the theme.customizer.color-scheme.js
if (!function_exists('tiger_claw_customizer_add_theme_colors')) {
	function tiger_claw_customizer_add_theme_colors($colors) {
		if (substr($colors['text'], 0, 1) == '#') {
			$colors['bg_color_0']  = tiger_claw_hex2rgba( $colors['bg_color'], 0 );
			$colors['bg_color_02']  = tiger_claw_hex2rgba( $colors['bg_color'], 0.2 );
			$colors['bg_color_07']  = tiger_claw_hex2rgba( $colors['bg_color'], 0.7 );
			$colors['bg_color_08']  = tiger_claw_hex2rgba( $colors['bg_color'], 0.8 );
			$colors['bg_color_09']  = tiger_claw_hex2rgba( $colors['bg_color'], 0.9 );
			$colors['alter_bg_color_07']  = tiger_claw_hex2rgba( $colors['alter_bg_color'], 0.7 );
			$colors['alter_bg_color_04']  = tiger_claw_hex2rgba( $colors['alter_bg_color'], 0.4 );
			$colors['alter_bg_color_02']  = tiger_claw_hex2rgba( $colors['alter_bg_color'], 0.2 );
			$colors['alter_bd_color_02']  = tiger_claw_hex2rgba( $colors['alter_bd_color'], 0.2 );
			$colors['extra_bg_color_07']  = tiger_claw_hex2rgba( $colors['extra_bg_color'], 0.7 );
			$colors['text_dark_07']  = tiger_claw_hex2rgba( $colors['text_dark'], 0.7 );
			$colors['text_link_02']  = tiger_claw_hex2rgba( $colors['text_link'], 0.2 );
			$colors['text_link_07']  = tiger_claw_hex2rgba( $colors['text_link'], 0.7 );
			$colors['text_link_blend'] = tiger_claw_hsb2hex(tiger_claw_hex2hsb( $colors['text_link'], 2, -5, 5 ));
			$colors['alter_link_blend'] = tiger_claw_hsb2hex(tiger_claw_hex2hsb( $colors['alter_link'], 2, -5, 5 ));
		} else {
			$colors['bg_color_0'] = '{{ data.bg_color_0 }}';
			$colors['bg_color_02'] = '{{ data.bg_color_02 }}';
			$colors['bg_color_07'] = '{{ data.bg_color_07 }}';
			$colors['bg_color_08'] = '{{ data.bg_color_08 }}';
			$colors['bg_color_09'] = '{{ data.bg_color_09 }}';
			$colors['alter_bg_color_07'] = '{{ data.alter_bg_color_07 }}';
			$colors['alter_bg_color_04'] = '{{ data.alter_bg_color_04 }}';
			$colors['alter_bg_color_02'] = '{{ data.alter_bg_color_02 }}';
			$colors['alter_bd_color_02'] = '{{ data.alter_bd_color_02 }}';
			$colors['extra_bg_color_07'] = '{{ data.extra_bg_color_07 }}';
			$colors['text_dark_07'] = '{{ data.text_dark_07 }}';
			$colors['text_link_02'] = '{{ data.text_link_02 }}';
			$colors['text_link_07'] = '{{ data.text_link_07 }}';
			$colors['text_link_blend'] = '{{ data.text_link_blend }}';
			$colors['alter_link_blend'] = '{{ data.alter_link_blend }}';
		}
		return $colors;
	}
}


			
// Additional theme-specific fonts rules
// Attention! Don't forget setup fonts rules also in the theme.customizer.color-scheme.js
if (!function_exists('tiger_claw_customizer_add_theme_fonts')) {
	function tiger_claw_customizer_add_theme_fonts($fonts) {
		$rez = array();	
		foreach ($fonts as $tag => $font) {
			if (substr($font['font-family'], 0, 2) != '{{') {
				$rez[$tag.'_font-family'] 		= !empty($font['font-family']) && !tiger_claw_is_inherit($font['font-family'])
														? 'font-family:' . trim($font['font-family']) . ';' 
														: '';
				$rez[$tag.'_font-size'] 		= !empty($font['font-size']) && !tiger_claw_is_inherit($font['font-size'])
														? 'font-size:' . tiger_claw_prepare_css_value($font['font-size']) . ";"
														: '';
				$rez[$tag.'_line-height'] 		= !empty($font['line-height']) && !tiger_claw_is_inherit($font['line-height'])
														? 'line-height:' . trim($font['line-height']) . ";"
														: '';
				$rez[$tag.'_font-weight'] 		= !empty($font['font-weight']) && !tiger_claw_is_inherit($font['font-weight'])
														? 'font-weight:' . trim($font['font-weight']) . ";"
														: '';
				$rez[$tag.'_font-style'] 		= !empty($font['font-style']) && !tiger_claw_is_inherit($font['font-style'])
														? 'font-style:' . trim($font['font-style']) . ";"
														: '';
				$rez[$tag.'_text-decoration'] 	= !empty($font['text-decoration']) && !tiger_claw_is_inherit($font['text-decoration'])
														? 'text-decoration:' . trim($font['text-decoration']) . ";"
														: '';
				$rez[$tag.'_text-transform'] 	= !empty($font['text-transform']) && !tiger_claw_is_inherit($font['text-transform'])
														? 'text-transform:' . trim($font['text-transform']) . ";"
														: '';
				$rez[$tag.'_letter-spacing'] 	= !empty($font['letter-spacing']) && !tiger_claw_is_inherit($font['letter-spacing'])
														? 'letter-spacing:' . trim($font['letter-spacing']) . ";"
														: '';
				$rez[$tag.'_margin-top'] 		= !empty($font['margin-top']) && !tiger_claw_is_inherit($font['margin-top'])
														? 'margin-top:' . tiger_claw_prepare_css_value($font['margin-top']) . ";"
														: '';
				$rez[$tag.'_margin-bottom'] 	= !empty($font['margin-bottom']) && !tiger_claw_is_inherit($font['margin-bottom'])
														? 'margin-bottom:' . tiger_claw_prepare_css_value($font['margin-bottom']) . ";"
														: '';
			} else {
				$rez[$tag.'_font-family']		= '{{ data["'.$tag.'_font-family"] }}';
				$rez[$tag.'_font-size']			= '{{ data["'.$tag.'_font-size"] }}';
				$rez[$tag.'_line-height']		= '{{ data["'.$tag.'_line-height"] }}';
				$rez[$tag.'_font-weight']		= '{{ data["'.$tag.'_font-weight"] }}';
				$rez[$tag.'_font-style']		= '{{ data["'.$tag.'_font-style"] }}';
				$rez[$tag.'_text-decoration']	= '{{ data["'.$tag.'_text-decoration"] }}';
				$rez[$tag.'_text-transform']	= '{{ data["'.$tag.'_text-transform"] }}';
				$rez[$tag.'_letter-spacing']	= '{{ data["'.$tag.'_letter-spacing"] }}';
				$rez[$tag.'_margin-top']		= '{{ data["'.$tag.'_margin-top"] }}';
				$rez[$tag.'_margin-bottom']		= '{{ data["'.$tag.'_margin-bottom"] }}';
			}
		}
		return $rez;
	}
}


//-------------------------------------------------------
//-- Thumb sizes
//-------------------------------------------------------

if ( !function_exists('tiger_claw_customizer_theme_setup') ) {
	add_action( 'after_setup_theme', 'tiger_claw_customizer_theme_setup' );
	function tiger_claw_customizer_theme_setup() {

		// Enable support for Post Thumbnails
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size(370, 0, false);
		
		// Add thumb sizes
		// ATTENTION! If you change list below - check filter's names in the 'trx_addons_filter_get_thumb_size' hook
		$thumb_sizes = apply_filters('tiger_claw_filter_add_thumb_sizes', array(
			'tiger_claw-thumb-huge'		=> array(1170, 658, true),
			'tiger_claw-thumb-big' 		=> array( 770, 433, true),
			'tiger_claw-thumb-med' 		=> array( 370, 208, true),
			'tiger_claw-thumb-tiny' 		=> array(  98,  88, true), //ok
			'tiger_claw-thumb-masonry-big' => array( 760,   0, false),		// Only downscale, not crop
			'tiger_claw-thumb-masonry'		=> array( 370,   0, false),		// Only downscale, not crop
			'tiger_claw-thumb-extra'		=> array( 370,  335, true),
			'tiger_claw-thumb-large'		=> array( 900,  843, true),
			)
		);
		$mult = tiger_claw_get_theme_option('retina_ready', 1);
		if ($mult > 1) $GLOBALS['content_width'] = apply_filters( 'tiger_claw_filter_content_width', 1170*$mult);
		foreach ($thumb_sizes as $k=>$v) {
			// Add Original dimensions
			add_image_size( $k, $v[0], $v[1], $v[2]);
			// Add Retina dimensions
			if ($mult > 1) add_image_size( $k.'-@retina', $v[0]*$mult, $v[1]*$mult, $v[2]);
		}

	}
}

if ( !function_exists('tiger_claw_customizer_image_sizes') ) {
	add_filter( 'image_size_names_choose', 'tiger_claw_customizer_image_sizes' );
	function tiger_claw_customizer_image_sizes( $sizes ) {
		$thumb_sizes = apply_filters('tiger_claw_filter_add_thumb_sizes', array(
			'tiger_claw-thumb-huge'		=> esc_html__( 'Fullsize image', 'tiger-claw' ),
			'tiger_claw-thumb-big'			=> esc_html__( 'Large image', 'tiger-claw' ),
			'tiger_claw-thumb-med'			=> esc_html__( 'Medium image', 'tiger-claw' ),
			'tiger_claw-thumb-tiny'		=> esc_html__( 'Small square avatar', 'tiger-claw' ),
			'tiger_claw-thumb-masonry-big'	=> esc_html__( 'Masonry Large (scaled)', 'tiger-claw' ),
			'tiger_claw-thumb-masonry'		=> esc_html__( 'Masonry (scaled)', 'tiger-claw' ),
			'tiger_claw-thumb-extra'		=> esc_html__( 'Extra', 'tiger-claw' ),
			'tiger_claw-thumb-large'		=> esc_html__( 'Large', 'tiger-claw' ),
			)
		);
		$mult = tiger_claw_get_theme_option('retina_ready', 1);
		foreach($thumb_sizes as $k=>$v) {
			$sizes[$k] = $v;
			if ($mult > 1) $sizes[$k.'-@retina'] = $v.' '.esc_html__('@2x', 'tiger-claw' );
		}
		return $sizes;
	}
}

// Remove some thumb-sizes from the ThemeREX Addons list
if ( !function_exists( 'tiger_claw_customizer_trx_addons_add_thumb_sizes' ) ) {
	add_filter( 'trx_addons_filter_add_thumb_sizes', 'tiger_claw_customizer_trx_addons_add_thumb_sizes');
	function tiger_claw_customizer_trx_addons_add_thumb_sizes($list=array()) {
		if (is_array($list)) {
			foreach ($list as $k=>$v) {
				if (in_array($k, array(
								'trx_addons-thumb-huge',
								'trx_addons-thumb-big',
								'trx_addons-thumb-medium',
								'trx_addons-thumb-tiny',
								'trx_addons-thumb-masonry-big',
								'trx_addons-thumb-masonry',
								'trx_addons-thumb-extra',
								'trx_addons-thumb-large',
								)
							)
						) unset($list[$k]);
			}
		}
		return $list;
	}
}

// and replace removed styles with theme-specific thumb size
if ( !function_exists( 'tiger_claw_customizer_trx_addons_get_thumb_size' ) ) {
	add_filter( 'trx_addons_filter_get_thumb_size', 'tiger_claw_customizer_trx_addons_get_thumb_size');
	function tiger_claw_customizer_trx_addons_get_thumb_size($thumb_size='') {
		return str_replace(array(
							'trx_addons-thumb-huge',
							'trx_addons-thumb-huge-@retina',
							'trx_addons-thumb-big',
							'trx_addons-thumb-big-@retina',
							'trx_addons-thumb-medium',
							'trx_addons-thumb-medium-@retina',
							'trx_addons-thumb-tiny',
							'trx_addons-thumb-tiny-@retina',
							'trx_addons-thumb-masonry-big',
							'trx_addons-thumb-masonry-big-@retina',
							'trx_addons-thumb-masonry',
							'trx_addons-thumb-masonry-@retina',
							'trx_addons-thumb-extra',
							'trx_addons-thumb-extra-@retina',
							'trx_addons-thumb-large',
							'trx_addons-thumb-large-@retina',
							),
							array(
							'tiger_claw-thumb-huge',
							'tiger_claw-thumb-huge-@retina',
							'tiger_claw-thumb-big',
							'tiger_claw-thumb-big-@retina',
							'tiger_claw-thumb-med',
							'tiger_claw-thumb-med-@retina',
							'tiger_claw-thumb-tiny',
							'tiger_claw-thumb-tiny-@retina',
							'tiger_claw-thumb-masonry-big',
							'tiger_claw-thumb-masonry-big-@retina',
							'tiger_claw-thumb-masonry',
							'tiger_claw-thumb-masonry-@retina',
							'tiger_claw-thumb-extra',
							'tiger_claw-thumb-extra-@retina',
							'tiger_claw-thumb-large',
							'tiger_claw-thumb-large-@retina',
							),
							$thumb_size);
	}
}
?>