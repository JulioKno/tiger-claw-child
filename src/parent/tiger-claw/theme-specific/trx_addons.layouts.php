<?php
//Custom Layouts
$layouts = array(
		'custom_394' => array(
				'name' => 'About Our Club',
				'template' => '[vc_row][vc_column width=\"1/2\" icons_position=\"left\"][vc_empty_space height=\"1.4em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_sc_title title_style=\"default\" title_tag=\"h2\" title=\"About our Club\"][vc_empty_space height=\"2em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_empty_space height=\"1.2em\" alter_height=\"none\" hide_on_mobile=\"1\"][trx_sc_content size=\"100p\" number_position=\"br\" title_style=\"default\" css=\".vc_custom_1496320675264{background-color: #f2f2f2 !important;}\" class=\"extra_margin_about\"][ess_grid alias=\"about\"][/trx_sc_content][trx_sc_content size=\"100p\" align=\"right\" number_position=\"br\" title_style=\"default\" class=\"extra_margin_about_link\"][trx_sc_button type=\"simple\" icon=\"icon-play\" icon_position=\"left\" link=\"/gallery-grid/\" title=\"More photos\"][trx_sc_button type=\"simple\" icon=\"icon-play\" icon_position=\"left\" link=\"/our-team/\" title=\"More about us\"][/trx_sc_content][vc_empty_space height=\"3em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column][vc_column width=\"1/2\" icons_position=\"left\"][vc_single_image image=\"377\" img_size=\"full\" css=\".vc_custom_1496321764985{margin-left: -2em !important;}\" el_class=\"remove_left_margin\"][vc_empty_space height=\"2.6em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_sc_content size=\"90p\" float=\"center\" align=\"left\" number_position=\"br\" title_style=\"default\"][vc_column_text css=\".vc_custom_1496322255937{padding-right: 10px !important;padding-left: 3px !important;}\"]
<p style=\"margin-bottom: 1.3em;\">Martial arts are codified systems and traditions of combat practices, which are practiced for a number of reasons: as self-defense, military and law enforcement applications, mental and spiritual development, and cultural heritage.</p>

<img class=\"size-full wp-image-378 alignright\" src=\"http://tiger-claw.themerex.net/wp-content/uploads/2017/05/sign.png\" alt=\"\" width=\"251\" height=\"84\" />[/vc_column_text][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1496320675264{background-color: #f2f2f2 !important;}.vc_custom_1496321764985{margin-left: -2em !important;}.vc_custom_1496322255937{padding-right: 10px !important;padding-left: 3px !important;}'
						)
				),
		'custom_288' => array(
				'name' => 'Clients',
				'template' => '[vc_row][vc_column][trx_widget_slider engine=\"swiper\" slides_type=\"images\" noresize=\"\" effect=\"slide\" direction=\"horizontal\" slides_per_view=\"6\" interval=\"5000\" controls=\"\" pagination=\"\" titles=\"center\" large=\"\" category=\"0\" slides=\"%5B%7B%22image%22%3A%22286%22%7D%2C%7B%22image%22%3A%22285%22%7D%2C%7B%22image%22%3A%22284%22%7D%2C%7B%22image%22%3A%22283%22%7D%2C%7B%22image%22%3A%22282%22%7D%2C%7B%22image%22%3A%22281%22%7D%5D\"][/trx_widget_slider][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								)
						)
				),
		'footer_417' => array(
				'name' => 'Footer',
				'template' => '[vc_row row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1496157770069{background-color: #212226 !important;}\"][vc_column icons_position=\"left\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\"][vc_empty_space height=\"1.8em\" alter_height=\"none\" hide_on_mobile=\"1\"][vc_empty_space height=\"4em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_sc_title title_style=\"default\" title_tag=\"h5\" title_align=\"center\" title=\"Newsletter\"][vc_empty_space height=\"2.2em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_column_text][mc4wp_form id=\"454\"][/vc_column_text][vc_empty_space height=\"0.65em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_widget_socials align=\"center\"][vc_wp_text el_class=\"extra_color_link\"]
<p style=\"text-align: center; font-size: 0.938em;\"><span style=\"color: #ffffff;\"><a style=\"color: #ffffff;\" href=\"https://themeforest.net/user/axiomthemes/portfolio\">Tiger Claw</a> &copy; 2017. All Rights Reserved. <a style=\"color: #ffffff;\" href=\"http://axiomthemes.com/terms-of-service/\">Terms of Use</a> and <a style=\"color: #ffffff;\" href=\"http://axiomthemes.com/privacy-policy/\">Privacy Policy</a></span></p>

[/vc_wp_text][vc_empty_space height=\"4em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_empty_space height=\"1em\" alter_height=\"none\" hide_on_mobile=\"1\"][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'footer'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1496157770069{background-color: #212226 !important;}'
						)
				),
		'header_419' => array(
				'name' => 'Header 1',
				'template' => '[vc_row scheme=\"dark\" row_type=\"narrow\" row_delimiter=\"\" row_fixed=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\"][vc_column width=\"1/12\" column_align=\"center\" icons_position=\"left\" column_type=\"center\"][/vc_column][vc_column width=\"10/12\" icons_position=\"left\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner width=\"1/2\" column_align=\"left\" icons_position=\"left\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"350 5th Ave, New York, NY 10118, USA\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"Mon - Fri : 09:00 - 17:00\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"0 (800) 123 45 67\"][/vc_column_inner][vc_column_inner width=\"1/2\" column_align=\"right\" icons_position=\"left\"][trx_sc_button type=\"default\" icon_position=\"left\" link=\"/classes-page/\" title=\"BOOK NOW\" icon_type=\"fontawesome\" icon_fontawesome=\"\"][trx_widget_socials align=\"center\"][trx_sc_layouts_login hide_on_mobile=\"1\" text_login=\"Login or Register\" text_logout=\"Logout\"][trx_sc_layouts_cart text=\"Shopping Cart\"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width=\"1/12\" icons_position=\"left\"][/vc_column][/vc_row][vc_row scheme=\"dark\" row_type=\"normal\" row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1496137090785{background: #d64601 url(http://tiger-claw.themerex.net/wp-content/uploads/2017/05/top_bg_header.jpg?id=461) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column width=\"1/12\" icons_position=\"left\" column_type=\"center\"][/vc_column][vc_column width=\"10/12\" icons_position=\"left\"][vc_row_inner content_placement=\"middle\" row_delimiter=\"\"][vc_column_inner width=\"1/4\" column_align=\"left\" icons_position=\"left\"][vc_empty_space height=\"0.8em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_sc_layouts_logo logo=\"420\"][/vc_column_inner][vc_column_inner width=\"3/4\" column_align=\"right\" icons_position=\"left\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\" class=\"extra_margin_right\"][/vc_column_inner][/vc_row_inner][vc_empty_space height=\"3.9em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_sc_content size=\"100p\" float=\"center\" align=\"center\" number_position=\"br\" title_style=\"default\"][trx_sc_layouts_title title=\"1\" meta=\"\" breadcrumbs=\"1\" icon_type=\"fontawesome\" icon_fontawesome=\"\"][/trx_sc_content][vc_empty_space height=\"4em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_empty_space height=\"3.35em\" alter_height=\"none\" hide_on_mobile=\"1\"][/vc_column][vc_column width=\"1/12\" icons_position=\"left\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1496137090785{background: #d64601 url(http://tiger-claw.themerex.net/wp-content/uploads/2017/05/top_bg_header.jpg?id=461) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}'
						)
				),
		'header_464' => array(
				'name' => 'Header 2',
				'template' => '[vc_row scheme=\"dark\" row_type=\"narrow\" row_delimiter=\"\" row_fixed=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1496154826311{background-color: rgba(33,34,38,0.3) !important;*background-color: rgb(33,34,38) !important;}\"][vc_column width=\"1/12\" column_align=\"center\" icons_position=\"left\" column_type=\"center\"][/vc_column][vc_column width=\"10/12\" icons_position=\"left\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner width=\"1/2\" column_align=\"left\" icons_position=\"left\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"350 5th Ave, New York, NY 10118, USA\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"Mon - Fri : 09:00 - 17:00\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"0 (800) 123 45 67\"][/vc_column_inner][vc_column_inner width=\"1/2\" column_align=\"right\" icons_position=\"left\"][trx_sc_button type=\"default\" icon_position=\"left\" link=\"/classes-page/\" title=\"BOOK NOW\" icon_type=\"fontawesome\" icon_fontawesome=\"\"][trx_widget_socials align=\"center\"][trx_sc_layouts_login hide_on_mobile=\"1\" text_login=\"Login or Register\" text_logout=\"Logout\"][trx_sc_layouts_cart text=\"Shopping Cart\"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width=\"1/12\" icons_position=\"left\"][/vc_column][/vc_row][vc_row scheme=\"dark\" row_type=\"normal\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\"][vc_column width=\"1/12\" icons_position=\"left\" column_type=\"center\"][/vc_column][vc_column width=\"10/12\" icons_position=\"left\"][vc_row_inner content_placement=\"middle\" row_delimiter=\"\"][vc_column_inner width=\"1/4\" column_align=\"left\" icons_position=\"left\"][vc_empty_space height=\"0.8em\" alter_height=\"none\" hide_on_mobile=\"1\"][trx_sc_layouts_logo logo=\"420\"][vc_empty_space height=\"0.8em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column_inner][vc_column_inner width=\"3/4\" column_align=\"right\" icons_position=\"left\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\" class=\"extra_margin_right\"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width=\"1/12\" icons_position=\"left\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1496154826311{background-color: rgba(33,34,38,0.3) !important;*background-color: rgb(33,34,38) !important;}'
						)
				),
		'header_702' => array(
				'name' => 'Header Box',
				'template' => '[vc_row scheme=\"dark\" row_type=\"narrow\" row_delimiter=\"\" row_fixed=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1496769108274{background-color: #191919 !important;}\"][vc_column column_align=\"center\" icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner width=\"1/2\" column_align=\"left\" icons_position=\"left\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"350 5th Ave, New York, NY 10118, USA\"][/vc_column_inner][vc_column_inner width=\"1/2\" column_align=\"right\" icons_position=\"left\"][trx_sc_button type=\"default\" icon_position=\"left\" link=\"/classes/\" title=\"BOOK NOW\" icon_type=\"fontawesome\" icon_fontawesome=\"\"][trx_sc_layouts_cart text=\"Shopping Cart\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row][vc_row scheme=\"dark\" row_type=\"normal\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1496927504362{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\"][vc_row_inner content_placement=\"middle\" row_delimiter=\"\"][vc_column_inner width=\"1/4\" column_align=\"left\" icons_position=\"left\"][vc_empty_space height=\"0.8em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_sc_layouts_logo logo=\"420\"][vc_empty_space height=\"0.8em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column_inner][vc_column_inner width=\"3/4\" column_align=\"right\" icons_position=\"left\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\" class=\"extra_margin_right\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1496769108274{background-color: #191919 !important;}.vc_custom_1496927504362{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}'
						)
				),
		'header_655' => array(
				'name' => 'Header Main',
				'template' => '[vc_row scheme=\"dark\" row_type=\"narrow\" row_delimiter=\"\" row_fixed=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\"][vc_column width=\"1/12\" column_align=\"center\" icons_position=\"left\" column_type=\"center\"][/vc_column][vc_column width=\"10/12\" icons_position=\"left\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner width=\"1/2\" column_align=\"left\" icons_position=\"left\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"350 5th Ave, New York, NY 10118, USA\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"Mon - Fri : 09:00 - 17:00\"][trx_sc_layouts_iconed_text hide_on_mobile=\"1\" text1=\"0 (800) 123 45 67\"][/vc_column_inner][vc_column_inner width=\"1/2\" column_align=\"right\" icons_position=\"left\"][trx_sc_button type=\"default\" icon_position=\"left\" link=\"/classes-page/\" title=\"BOOK NOW\" icon_type=\"fontawesome\" icon_fontawesome=\"\"][trx_widget_socials align=\"center\"][trx_sc_layouts_login hide_on_mobile=\"1\" text_login=\"Login or Register\" text_logout=\"Logout\"][trx_sc_layouts_cart text=\"Shopping Cart\"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width=\"1/12\" icons_position=\"left\"][/vc_column][/vc_row][vc_row scheme=\"dark\" row_type=\"normal\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1496926873242{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column width=\"1/12\" icons_position=\"left\" column_type=\"center\"][/vc_column][vc_column width=\"10/12\" icons_position=\"left\"][vc_row_inner content_placement=\"middle\" row_delimiter=\"\"][vc_column_inner width=\"1/4\" column_align=\"left\" icons_position=\"left\"][vc_empty_space height=\"0.8em\" alter_height=\"none\" hide_on_mobile=\"1\"][trx_sc_layouts_logo logo=\"420\"][vc_empty_space height=\"0.8em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column_inner][vc_column_inner width=\"3/4\" column_align=\"right\" icons_position=\"left\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\" class=\"extra_margin_right\"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width=\"1/12\" icons_position=\"left\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1496926873242{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}'
						)
				),
		'custom_406' => array(
				'name' => 'Information',
				'template' => '[vc_row][vc_column width=\"1/3\" icons_position=\"left\"][vc_column_text]
<h5 style=\"text-align: center;\">About Us</h5>
<p style=\"text-align: center; font-size: 0.875em;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eget nisl luctus, malesuada eros ut, eleifend justo donec venenatis ligula nibh.</p>
[/vc_column_text][vc_empty_space height=\"2em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_empty_space height=\"1.5em\" alter_height=\"none\" hide_on_mobile=\"1,3\"][trx_sc_button type=\"default\" size=\"small\" align=\"center\" icon_position=\"left\" link=\"/about-studio/\" title=\"view more\"][vc_empty_space height=\"3em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][vc_column_text]
<h5 style=\"text-align: center;\">Open Hours</h5>
<p style=\"text-align: center; font-size: 0.875em;\">Monday: 11am-7pm
Tuesday-Friday: 11am-8pm
Saturday: 10am-6pm
Sunday: 11am-6pm</p>
[/vc_column_text][vc_empty_space height=\"2em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_sc_button type=\"default\" size=\"small\" align=\"center\" icon_position=\"left\" link=\"/classes/\" title=\"book now\"][vc_empty_space height=\"3em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][vc_column_text]
<h5 style=\"text-align: center;\">Contacts</h5>
<p style=\"text-align: center; font-size: 0.875em;\">350 5th Ave, New York, NY 10118
Email: <a href=\"mailto:example@email.com\">info@yoursite.com</a>
Telephone: +1 (800)-123-4567</p>
[/vc_column_text][vc_empty_space height=\"2em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_empty_space height=\"1.5em\" alter_height=\"none\" hide_on_mobile=\"1\"][trx_sc_button type=\"default\" size=\"small\" align=\"center\" icon_position=\"left\" link=\"/contacts/\" title=\"open map\"][vc_empty_space height=\"3em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								)
						)
				),
		'custom_269' => array(
				'name' => 'Price Block',
				'template' => '[vc_row full_width=\"stretch_row\" css=\".vc_custom_1494594758519{background-image: url(http://tiger-claw.themerex.net/wp-content/uploads/2017/05/bg_price.jpg?id=270) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column icons_position=\"left\"][vc_empty_space height=\"4em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_empty_space height=\"3.5em\" alter_height=\"none\" hide_on_mobile=\"1\"][trx_sc_title title_style=\"default\" title_tag=\"h2\" title_align=\"center\" title=\"Choose Your Membership\"][vc_empty_space height=\"3.4em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_empty_space height=\"1em\" alter_height=\"none\" hide_on_mobile=\"1\"][vc_row_inner][vc_column_inner width=\"1/3\" icons_position=\"left\"][trx_sc_price icon_type=\"fontawesome\" title=\"Casual Visit\" price=\"$25./each\" link=\"#\" link_text=\"choose plan\"][vc_column_text]
<p style=\"text-align: center;\">Fifo - Fly In / Fly Out
No Contract

[/vc_column_text][/trx_sc_price][vc_empty_space height=\"2.5em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column_inner][vc_column_inner width=\"1/3\" icons_position=\"left\"][trx_sc_price icon_type=\"fontawesome\" title=\"The Ultimate\" price=\"$99./monthly\" link=\"#\" link_text=\"choose plan\"][vc_column_text]
<p style=\"text-align: center;\">Membership
Unlimited Training
Access to all classes
Shirt &amp; Pants
Glovers &amp; Fightgear

[/vc_column_text][/trx_sc_price][vc_empty_space height=\"2.5em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column_inner][vc_column_inner width=\"1/3\" icons_position=\"left\"][trx_sc_price icon_type=\"fontawesome\" title=\"Gym Only\" price=\"$50./monthly\" link=\"#\" link_text=\"choose plan\"][vc_column_text]
<p style=\"text-align: center;\">Fifo - Fly In / Fly Out
Train 24 hrs/day

[/vc_column_text][/trx_sc_price][vc_empty_space height=\"2.5em\" alter_height=\"none\" hide_on_mobile=\"\"][/vc_column_inner][/vc_row_inner][vc_empty_space height=\"3.8em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_empty_space height=\"3em\" alter_height=\"none\" hide_on_mobile=\"1\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494594758519{background-image: url(http://tiger-claw.themerex.net/wp-content/uploads/2017/05/bg_price.jpg?id=270) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}'
						)
				),
		'custom_279' => array(
				'name' => 'Testimonials',
				'template' => '[vc_row][vc_column icons_position=\"left\"][trx_sc_title title_style=\"default\" title_tag=\"h2\" title_align=\"center\" title=\"Our Happy Clients\" description=\"Testimonials\"][vc_empty_space height=\"2.9em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_sc_testimonials type=\"default\" orderby=\"post_date\" order=\"desc\" title_style=\"default\" title_align=\"center\" count=\"2\" columns=\"2\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								)
						)
				),
		'custom_386' => array(
				'name' => 'Video Block',
				'template' => '[vc_row][vc_column icons_position=\"left\"][trx_sc_promo title_style=\"default\" title_tag=\"h1\" title_align=\"left\" image_position=\"right\" image_width=\"60%\" video_in_popup=\"\" size=\"large\" full_height=\"\" text_paddings=\"\" title=\"Train for free today\" image=\"383\" text_margins=\"1.5em 0 3.5em 0.4em\"][/trx_sc_promo][trx_sc_promo title_style=\"default\" title_align=\"left\" image_position=\"left\" image_width=\"40%\" video_in_popup=\"1\" size=\"normal\" full_height=\"\" text_paddings=\"\" image=\"31\" video_url=\"https://vimeo.com/66047650\" text_margins=\"2.7em 2.7em 0 0\"][trx_sc_title title_style=\"default\" title_tag=\"h4\" title=\"Change Happens Here.\"][trx_sc_title title_style=\"default\" title_tag=\"h4\" title=\"Tired of your fitness routine? So are we.\"][vc_empty_space height=\"1.1em\" alter_height=\"none\" hide_on_mobile=\"\"][vc_column_text]Sed rutrum leo ante, vel lobortis odio pellentesque eu. Suspendisse faucibus elementum pharetra. Aenean quis vehicula dolor. Sed condimentum interdum convallis. Nam quam arcu, eleifend vel ultrices ut, sollicitudin ut sem.[/vc_column_text][vc_empty_space height=\"1.5em\" alter_height=\"none\" hide_on_mobile=\"\"][trx_sc_button type=\"default\" size=\"small\" icon_position=\"left\" link=\"/upcoming-events/\" title=\"get a free pass\"][/trx_sc_promo][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								)
						)
				)
		);
?>