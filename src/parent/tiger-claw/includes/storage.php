<?php
/**
 * Theme storage manipulations
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get theme variable
if (!function_exists('tiger_claw_storage_get')) {
	function tiger_claw_storage_get($var_name, $default='') {
		global $TIGER_CLAW_STORAGE;
		return isset($TIGER_CLAW_STORAGE[$var_name]) ? $TIGER_CLAW_STORAGE[$var_name] : $default;
	}
}

// Set theme variable
if (!function_exists('tiger_claw_storage_set')) {
	function tiger_claw_storage_set($var_name, $value) {
		global $TIGER_CLAW_STORAGE;
		$TIGER_CLAW_STORAGE[$var_name] = $value;
	}
}

// Check if theme variable is empty
if (!function_exists('tiger_claw_storage_empty')) {
	function tiger_claw_storage_empty($var_name, $key='', $key2='') {
		global $TIGER_CLAW_STORAGE;
		if (!empty($key) && !empty($key2))
			return empty($TIGER_CLAW_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return empty($TIGER_CLAW_STORAGE[$var_name][$key]);
		else
			return empty($TIGER_CLAW_STORAGE[$var_name]);
	}
}

// Check if theme variable is set
if (!function_exists('tiger_claw_storage_isset')) {
	function tiger_claw_storage_isset($var_name, $key='', $key2='') {
		global $TIGER_CLAW_STORAGE;
		if (!empty($key) && !empty($key2))
			return isset($TIGER_CLAW_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return isset($TIGER_CLAW_STORAGE[$var_name][$key]);
		else
			return isset($TIGER_CLAW_STORAGE[$var_name]);
	}
}

// Inc/Dec theme variable with specified value
if (!function_exists('tiger_claw_storage_inc')) {
	function tiger_claw_storage_inc($var_name, $value=1) {
		global $TIGER_CLAW_STORAGE;
		if (empty($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = 0;
		$TIGER_CLAW_STORAGE[$var_name] += $value;
	}
}

// Concatenate theme variable with specified value
if (!function_exists('tiger_claw_storage_concat')) {
	function tiger_claw_storage_concat($var_name, $value) {
		global $TIGER_CLAW_STORAGE;
		if (empty($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = '';
		$TIGER_CLAW_STORAGE[$var_name] .= $value;
	}
}

// Get array (one or two dim) element
if (!function_exists('tiger_claw_storage_get_array')) {
	function tiger_claw_storage_get_array($var_name, $key, $key2='', $default='') {
		global $TIGER_CLAW_STORAGE;
		if (empty($key2))
			return !empty($var_name) && !empty($key) && isset($TIGER_CLAW_STORAGE[$var_name][$key]) ? $TIGER_CLAW_STORAGE[$var_name][$key] : $default;
		else
			return !empty($var_name) && !empty($key) && isset($TIGER_CLAW_STORAGE[$var_name][$key][$key2]) ? $TIGER_CLAW_STORAGE[$var_name][$key][$key2] : $default;
	}
}

// Set array element
if (!function_exists('tiger_claw_storage_set_array')) {
	function tiger_claw_storage_set_array($var_name, $key, $value) {
		global $TIGER_CLAW_STORAGE;
		if (!isset($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = array();
		if ($key==='')
			$TIGER_CLAW_STORAGE[$var_name][] = $value;
		else
			$TIGER_CLAW_STORAGE[$var_name][$key] = $value;
	}
}

// Set two-dim array element
if (!function_exists('tiger_claw_storage_set_array2')) {
	function tiger_claw_storage_set_array2($var_name, $key, $key2, $value) {
		global $TIGER_CLAW_STORAGE;
		if (!isset($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = array();
		if (!isset($TIGER_CLAW_STORAGE[$var_name][$key])) $TIGER_CLAW_STORAGE[$var_name][$key] = array();
		if ($key2==='')
			$TIGER_CLAW_STORAGE[$var_name][$key][] = $value;
		else
			$TIGER_CLAW_STORAGE[$var_name][$key][$key2] = $value;
	}
}

// Merge array elements
if (!function_exists('tiger_claw_storage_merge_array')) {
	function tiger_claw_storage_merge_array($var_name, $key, $value) {
		global $TIGER_CLAW_STORAGE;
		if (!isset($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = array();
		if ($key==='')
			$TIGER_CLAW_STORAGE[$var_name] = array_merge($TIGER_CLAW_STORAGE[$var_name], $value);
		else
			$TIGER_CLAW_STORAGE[$var_name][$key] = array_merge($TIGER_CLAW_STORAGE[$var_name][$key], $value);
	}
}

// Add array element after the key
if (!function_exists('tiger_claw_storage_set_array_after')) {
	function tiger_claw_storage_set_array_after($var_name, $after, $key, $value='') {
		global $TIGER_CLAW_STORAGE;
		if (!isset($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = array();
		if (is_array($key))
			tiger_claw_array_insert_after($TIGER_CLAW_STORAGE[$var_name], $after, $key);
		else
			tiger_claw_array_insert_after($TIGER_CLAW_STORAGE[$var_name], $after, array($key=>$value));
	}
}

// Add array element before the key
if (!function_exists('tiger_claw_storage_set_array_before')) {
	function tiger_claw_storage_set_array_before($var_name, $before, $key, $value='') {
		global $TIGER_CLAW_STORAGE;
		if (!isset($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = array();
		if (is_array($key))
			tiger_claw_array_insert_before($TIGER_CLAW_STORAGE[$var_name], $before, $key);
		else
			tiger_claw_array_insert_before($TIGER_CLAW_STORAGE[$var_name], $before, array($key=>$value));
	}
}

// Push element into array
if (!function_exists('tiger_claw_storage_push_array')) {
	function tiger_claw_storage_push_array($var_name, $key, $value) {
		global $TIGER_CLAW_STORAGE;
		if (!isset($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = array();
		if ($key==='')
			array_push($TIGER_CLAW_STORAGE[$var_name], $value);
		else {
			if (!isset($TIGER_CLAW_STORAGE[$var_name][$key])) $TIGER_CLAW_STORAGE[$var_name][$key] = array();
			array_push($TIGER_CLAW_STORAGE[$var_name][$key], $value);
		}
	}
}

// Pop element from array
if (!function_exists('tiger_claw_storage_pop_array')) {
	function tiger_claw_storage_pop_array($var_name, $key='', $defa='') {
		global $TIGER_CLAW_STORAGE;
		$rez = $defa;
		if ($key==='') {
			if (isset($TIGER_CLAW_STORAGE[$var_name]) && is_array($TIGER_CLAW_STORAGE[$var_name]) && count($TIGER_CLAW_STORAGE[$var_name]) > 0) 
				$rez = array_pop($TIGER_CLAW_STORAGE[$var_name]);
		} else {
			if (isset($TIGER_CLAW_STORAGE[$var_name][$key]) && is_array($TIGER_CLAW_STORAGE[$var_name][$key]) && count($TIGER_CLAW_STORAGE[$var_name][$key]) > 0) 
				$rez = array_pop($TIGER_CLAW_STORAGE[$var_name][$key]);
		}
		return $rez;
	}
}

// Inc/Dec array element with specified value
if (!function_exists('tiger_claw_storage_inc_array')) {
	function tiger_claw_storage_inc_array($var_name, $key, $value=1) {
		global $TIGER_CLAW_STORAGE;
		if (!isset($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = array();
		if (empty($TIGER_CLAW_STORAGE[$var_name][$key])) $TIGER_CLAW_STORAGE[$var_name][$key] = 0;
		$TIGER_CLAW_STORAGE[$var_name][$key] += $value;
	}
}

// Concatenate array element with specified value
if (!function_exists('tiger_claw_storage_concat_array')) {
	function tiger_claw_storage_concat_array($var_name, $key, $value) {
		global $TIGER_CLAW_STORAGE;
		if (!isset($TIGER_CLAW_STORAGE[$var_name])) $TIGER_CLAW_STORAGE[$var_name] = array();
		if (empty($TIGER_CLAW_STORAGE[$var_name][$key])) $TIGER_CLAW_STORAGE[$var_name][$key] = '';
		$TIGER_CLAW_STORAGE[$var_name][$key] .= $value;
	}
}

// Call object's method
if (!function_exists('tiger_claw_storage_call_obj_method')) {
	function tiger_claw_storage_call_obj_method($var_name, $method, $param=null) {
		global $TIGER_CLAW_STORAGE;
		if ($param===null)
			return !empty($var_name) && !empty($method) && isset($TIGER_CLAW_STORAGE[$var_name]) ? $TIGER_CLAW_STORAGE[$var_name]->$method(): '';
		else
			return !empty($var_name) && !empty($method) && isset($TIGER_CLAW_STORAGE[$var_name]) ? $TIGER_CLAW_STORAGE[$var_name]->$method($param): '';
	}
}

// Get object's property
if (!function_exists('tiger_claw_storage_get_obj_property')) {
	function tiger_claw_storage_get_obj_property($var_name, $prop, $default='') {
		global $TIGER_CLAW_STORAGE;
		return !empty($var_name) && !empty($prop) && isset($TIGER_CLAW_STORAGE[$var_name]->$prop) ? $TIGER_CLAW_STORAGE[$var_name]->$prop : $default;
	}
}
?>