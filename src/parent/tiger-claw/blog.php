<?php
/**
 * The template to display blog archive
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

/*
Template Name: Blog archive
*/

/**
 * Make page with this template and put it into menu
 * to display posts as blog archive
 * You can setup output parameters (blog style, posts per page, parent category, etc.)
 * in the Theme Options section (under the page content)
 * You can build this page in the Visual Composer to make custom page layout:
 * just insert %%CONTENT%% in the desired place of content
 */

// Get template page's content
$tiger_claw_content = '';
$tiger_claw_blog_archive_mask = '%%CONTENT%%';
$tiger_claw_blog_archive_subst = sprintf('<div class="blog_archive">%s</div>', $tiger_claw_blog_archive_mask);
if ( have_posts() ) {
	the_post(); 
	if (($tiger_claw_content = apply_filters('the_content', get_the_content())) != '') {
		if (($tiger_claw_pos = strpos($tiger_claw_content, $tiger_claw_blog_archive_mask)) !== false) {
			$tiger_claw_content = preg_replace('/(\<p\>\s*)?'.$tiger_claw_blog_archive_mask.'(\s*\<\/p\>)/i', $tiger_claw_blog_archive_subst, $tiger_claw_content);
		} else
			$tiger_claw_content .= $tiger_claw_blog_archive_subst;
		$tiger_claw_content = explode($tiger_claw_blog_archive_mask, $tiger_claw_content);
		// Add VC custom styles to the inline CSS
		$vc_custom_css = get_post_meta( get_the_ID(), '_wpb_shortcodes_custom_css', true );
		if ( !empty( $vc_custom_css ) ) tiger_claw_add_inline_css(strip_tags($vc_custom_css));
	}
}

// Prepare args for a new query
$tiger_claw_args = array(
	'post_status' => current_user_can('read_private_pages') && current_user_can('read_private_posts') ? array('publish', 'private') : 'publish'
);
$tiger_claw_args = tiger_claw_query_add_posts_and_cats($tiger_claw_args, '', tiger_claw_get_theme_option('post_type'), tiger_claw_get_theme_option('parent_cat'));
$tiger_claw_page_number = get_query_var('paged') ? get_query_var('paged') : (get_query_var('page') ? get_query_var('page') : 1);
if ($tiger_claw_page_number > 1) {
	$tiger_claw_args['paged'] = $tiger_claw_page_number;
	$tiger_claw_args['ignore_sticky_posts'] = true;
}
$tiger_claw_ppp = tiger_claw_get_theme_option('posts_per_page');
if ((int) $tiger_claw_ppp != 0)
	$tiger_claw_args['posts_per_page'] = (int) $tiger_claw_ppp;
// Make a new query
query_posts( $tiger_claw_args );
// Set a new query as main WP Query
$GLOBALS['wp_the_query'] = $GLOBALS['wp_query'];

// Set query vars in the new query!
if (is_array($tiger_claw_content) && count($tiger_claw_content) == 2) {
	set_query_var('blog_archive_start', $tiger_claw_content[0]);
	set_query_var('blog_archive_end', $tiger_claw_content[1]);
}

get_template_part('index');
?>