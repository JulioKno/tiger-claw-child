<?php
/**
 * The template for homepage posts with "Portfolio" style
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

tiger_claw_storage_set('blog_archive', true);

// Load scripts for both 'Gallery' and 'Portfolio' layouts!
wp_enqueue_script( 'imagesloaded' );
wp_enqueue_script( 'masonry' );
wp_enqueue_script( 'classie', tiger_claw_get_file_url('js/theme.gallery/classie.min.js'), array(), null, true );
wp_enqueue_script( 'tiger_claw-gallery-script', tiger_claw_get_file_url('js/theme.gallery/theme.gallery.js'), array(), null, true );

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$tiger_claw_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$tiger_claw_sticky_out = tiger_claw_get_theme_option('sticky_style')=='columns' 
							&& is_array($tiger_claw_stickies) && count($tiger_claw_stickies) > 0 && get_query_var( 'paged' ) < 1;
	
	// Show filters
	$tiger_claw_cat = tiger_claw_get_theme_option('parent_cat');
	$tiger_claw_post_type = tiger_claw_get_theme_option('post_type');
	$tiger_claw_taxonomy = tiger_claw_get_post_type_taxonomy($tiger_claw_post_type);
	$tiger_claw_show_filters = tiger_claw_get_theme_option('show_filters');
	$tiger_claw_tabs = array();
	if (!tiger_claw_is_off($tiger_claw_show_filters)) {
		$tiger_claw_args = array(
			'type'			=> $tiger_claw_post_type,
			'child_of'		=> $tiger_claw_cat,
			'orderby'		=> 'name',
			'order'			=> 'ASC',
			'hide_empty'	=> 1,
			'hierarchical'	=> 0,
			'exclude'		=> '',
			'number'		=> '',
			'taxonomy'		=> $tiger_claw_taxonomy,
			'pad_counts'	=> false
		);
		$tiger_claw_portfolio_list = get_terms($tiger_claw_args);
		if (is_array($tiger_claw_portfolio_list) && count($tiger_claw_portfolio_list) > 0) {
			$tiger_claw_tabs[$tiger_claw_cat] = esc_html__('All', 'tiger-claw');
			foreach ($tiger_claw_portfolio_list as $tiger_claw_term) {
				if (isset($tiger_claw_term->term_id)) $tiger_claw_tabs[$tiger_claw_term->term_id] = $tiger_claw_term->name;
			}
		}
	}
	if (count($tiger_claw_tabs) > 0) {
		$tiger_claw_portfolio_filters_ajax = true;
		$tiger_claw_portfolio_filters_active = $tiger_claw_cat;
		$tiger_claw_portfolio_filters_id = 'portfolio_filters';
		if (!is_customize_preview())
			wp_enqueue_script('jquery-ui-tabs', false, array('jquery', 'jquery-ui-core'), null, true);
		?>
		<div class="portfolio_filters tiger_claw_tabs tiger_claw_tabs_ajax">
			<ul class="portfolio_titles tiger_claw_tabs_titles">
				<?php
				foreach ($tiger_claw_tabs as $tiger_claw_id=>$tiger_claw_title) {
					?><li><a href="<?php echo esc_url(tiger_claw_get_hash_link(sprintf('#%s_%s_content', $tiger_claw_portfolio_filters_id, $tiger_claw_id))); ?>" data-tab="<?php echo esc_attr($tiger_claw_id); ?>"><?php echo esc_html($tiger_claw_title); ?></a></li><?php
				}
				?>
			</ul>
			<?php
			$tiger_claw_ppp = tiger_claw_get_theme_option('posts_per_page');
			if (tiger_claw_is_inherit($tiger_claw_ppp)) $tiger_claw_ppp = '';
			foreach ($tiger_claw_tabs as $tiger_claw_id=>$tiger_claw_title) {
				$tiger_claw_portfolio_need_content = $tiger_claw_id==$tiger_claw_portfolio_filters_active || !$tiger_claw_portfolio_filters_ajax;
				?>
				<div id="<?php echo esc_attr(sprintf('%s_%s_content', $tiger_claw_portfolio_filters_id, $tiger_claw_id)); ?>"
					class="portfolio_content tiger_claw_tabs_content"
					data-blog-template="<?php echo esc_attr(tiger_claw_storage_get('blog_template')); ?>"
					data-blog-style="<?php echo esc_attr(tiger_claw_get_theme_option('blog_style')); ?>"
					data-posts-per-page="<?php echo esc_attr($tiger_claw_ppp); ?>"
					data-post-type="<?php echo esc_attr($tiger_claw_post_type); ?>"
					data-taxonomy="<?php echo esc_attr($tiger_claw_taxonomy); ?>"
					data-cat="<?php echo esc_attr($tiger_claw_id); ?>"
					data-parent-cat="<?php echo esc_attr($tiger_claw_cat); ?>"
					data-need-content="<?php echo (false===$tiger_claw_portfolio_need_content ? 'true' : 'false'); ?>"
				>
					<?php
					if ($tiger_claw_portfolio_need_content) 
						tiger_claw_show_portfolio_posts(array(
							'cat' => $tiger_claw_id,
							'parent_cat' => $tiger_claw_cat,
							'taxonomy' => $tiger_claw_taxonomy,
							'post_type' => $tiger_claw_post_type,
							'page' => 1,
							'sticky' => $tiger_claw_sticky_out
							)
						);
					?>
				</div>
				<?php
			}
			?>
		</div>
		<?php
	} else {
		tiger_claw_show_portfolio_posts(array(
			'cat' => $tiger_claw_cat,
			'parent_cat' => $tiger_claw_cat,
			'taxonomy' => $tiger_claw_taxonomy,
			'post_type' => $tiger_claw_post_type,
			'page' => 1,
			'sticky' => $tiger_claw_sticky_out
			)
		);
	}

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>