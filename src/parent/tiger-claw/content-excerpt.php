<?php
/**
 * The default template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

$tiger_claw_post_format = get_post_format();
$tiger_claw_post_format = empty($tiger_claw_post_format) ? 'standard' : str_replace('post-format-', '', $tiger_claw_post_format);
$tiger_claw_animation = tiger_claw_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>"
	<?php post_class( 'post_item post_layout_excerpt post_format_'.esc_attr($tiger_claw_post_format) ); ?>
	<?php echo (!tiger_claw_is_off($tiger_claw_animation) ? ' data-animation="'.esc_attr(tiger_claw_get_animation_classes($tiger_claw_animation)).'"' : ''); ?>
	><?php

	// Sticky label
	if ( is_sticky() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}


	?>
	<div class="wrap_post_single_top">
		<?php
		do_action('tiger_claw_action_before_post_title');
		// Post title
		the_title( sprintf( '<h2 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		?>
	</div>
	<?php


	// Featured image
	tiger_claw_show_post_featured(array( 'thumb_size' => tiger_claw_get_thumb_size( strpos(tiger_claw_get_theme_option('body_style'), 'full')!==false ? 'full' : 'big' ) ));
	?>
	<div class="wrap_post_single">
	<?php
	// Title and post meta
	if (get_the_title() != '') {
		?>
		<div class="post_header entry-header">
			<?php

			do_action('tiger_claw_action_before_post_meta');

			// Post meta
			$tiger_claw_components = tiger_claw_is_inherit(tiger_claw_get_theme_option_from_meta('meta_parts'))
										? 'categories,date,counters,edit'
										: tiger_claw_array_get_keys_by_value(tiger_claw_get_theme_option('meta_parts'));
			$tiger_claw_counters = tiger_claw_is_inherit(tiger_claw_get_theme_option_from_meta('counters'))
										? 'views,likes,comments'
										: tiger_claw_array_get_keys_by_value(tiger_claw_get_theme_option('counters'));

			if (!empty($tiger_claw_components))
				tiger_claw_show_post_meta(apply_filters('tiger_claw_filter_post_meta_args', array(
					'components' => $tiger_claw_components,
					'counters' => $tiger_claw_counters,
					'seo' => false
					), 'excerpt', 1)
				);
			?>
		</div><!-- .post_header --><?php
	}

	// Post content
	?><div class="post_content entry-content"><?php
		if (tiger_claw_get_theme_option('blog_content') == 'fullpost') {
			// Post content area
			?><div class="post_content_inner"><?php
				the_content( '' );
			?></div><?php
			// Inner pages
			wp_link_pages( array(
				'before'      => '<div class="page_links"><span class="page_links_title">' . esc_html__( 'Pages:', 'tiger-claw' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'tiger-claw' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

		} else {

			$tiger_claw_show_learn_more = !in_array($tiger_claw_post_format, array('link', 'aside', 'status', 'quote'));

			// Post content area
			?><div class="post_content_inner"><?php
				if (has_excerpt()) {
					the_excerpt();
				} else if (strpos(get_the_content('!--more'), '!--more')!==false) {
					the_content( '' );
				} else if (in_array($tiger_claw_post_format, array('link', 'aside', 'status'))) {
					the_content();
				} else if ($tiger_claw_post_format == 'quote') {
					if (($quote = tiger_claw_get_tag(get_the_content(), '<blockquote>', '</blockquote>'))!='')
						tiger_claw_show_layout(wpautop($quote));
					else
						the_excerpt();
				} else if (substr(get_the_content(), 0, 1)!='[') {
					the_excerpt();
				}
			?></div><?php
			// More button
			if ( $tiger_claw_show_learn_more ) {
				?><p><a class="more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read more', 'tiger-claw'); ?></a></p><?php
			}

		}
	?></div><!-- .entry-content -->
	</div>
</article>