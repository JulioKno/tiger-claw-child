<?php
/**
 * The template for homepage posts with "Classic" style
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

tiger_claw_storage_set('blog_archive', true);

// Load scripts for 'Masonry' layout
if (substr(tiger_claw_get_theme_option('blog_style'), 0, 7) == 'masonry') {
	wp_enqueue_script( 'imagesloaded' );
	wp_enqueue_script( 'masonry' );
	wp_enqueue_script( 'classie', tiger_claw_get_file_url('js/theme.gallery/classie.min.js'), array(), null, true );
	wp_enqueue_script( 'tiger_claw-gallery-script', tiger_claw_get_file_url('js/theme.gallery/theme.gallery.js'), array(), null, true );
}

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$tiger_claw_classes = 'posts_container '
						. (substr(tiger_claw_get_theme_option('blog_style'), 0, 7) == 'classic' ? 'columns_wrap' : 'masonry_wrap');
	$tiger_claw_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$tiger_claw_sticky_out = tiger_claw_get_theme_option('sticky_style')=='columns' 
							&& is_array($tiger_claw_stickies) && count($tiger_claw_stickies) > 0 && get_query_var( 'paged' ) < 1;
	if ($tiger_claw_sticky_out) {
		?><div class="sticky_wrap columns_wrap"><?php	
	}
	if (!$tiger_claw_sticky_out) {
		if (tiger_claw_get_theme_option('first_post_large') && !is_paged() && !in_array(tiger_claw_get_theme_option('body_style'), array('fullwide', 'fullscreen'))) {
			the_post();
			get_template_part( 'content', 'excerpt' );
		}
		
		?><div class="<?php echo esc_attr($tiger_claw_classes); ?>"><?php
	}
	while ( have_posts() ) { the_post(); 
		if ($tiger_claw_sticky_out && !is_sticky()) {
			$tiger_claw_sticky_out = false;
			?></div><div class="<?php echo esc_attr($tiger_claw_classes); ?>"><?php
		}
		get_template_part( 'content', $tiger_claw_sticky_out && is_sticky() ? 'sticky' : 'classic' );
	}
	
	?></div><?php

	tiger_claw_show_pagination();

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>