<?php
/**
 * The Gallery template to display posts
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

$tiger_claw_blog_style = explode('_', tiger_claw_get_theme_option('blog_style'));
$tiger_claw_columns = empty($tiger_claw_blog_style[1]) ? 2 : max(2, $tiger_claw_blog_style[1]);
$tiger_claw_post_format = get_post_format();
$tiger_claw_post_format = empty($tiger_claw_post_format) ? 'standard' : str_replace('post-format-', '', $tiger_claw_post_format);
$tiger_claw_animation = tiger_claw_get_theme_option('blog_animation');
$tiger_claw_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_portfolio post_layout_gallery post_layout_gallery_'.esc_attr($tiger_claw_columns).' post_format_'.esc_attr($tiger_claw_post_format) ); ?>
	<?php echo (!tiger_claw_is_off($tiger_claw_animation) ? ' data-animation="'.esc_attr(tiger_claw_get_animation_classes($tiger_claw_animation)).'"' : ''); ?>
	data-size="<?php if (!empty($tiger_claw_image[1]) && !empty($tiger_claw_image[2])) echo intval($tiger_claw_image[1]) .'x' . intval($tiger_claw_image[2]); ?>"
	data-src="<?php if (!empty($tiger_claw_image[0])) echo esc_url($tiger_claw_image[0]); ?>"
	>

	<?php

	// Sticky label
	if ( is_sticky() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

	// Featured image
	$tiger_claw_image_hover = 'icon';
	if (in_array($tiger_claw_image_hover, array('icons', 'zoom'))) $tiger_claw_image_hover = 'dots';
	$tiger_claw_components = tiger_claw_is_inherit(tiger_claw_get_theme_option_from_meta('meta_parts')) 
								? 'categories,date,counters,share'
								: tiger_claw_array_get_keys_by_value(tiger_claw_get_theme_option('meta_parts'));
	$tiger_claw_counters = tiger_claw_is_inherit(tiger_claw_get_theme_option_from_meta('counters')) 
								? 'comments'
								: tiger_claw_array_get_keys_by_value(tiger_claw_get_theme_option('counters'));
	tiger_claw_show_post_featured(array(
		'hover' => $tiger_claw_image_hover,
		'thumb_size' => tiger_claw_get_thumb_size( strpos(tiger_claw_get_theme_option('body_style'), 'full')!==false || $tiger_claw_columns < 3 ? 'masonry-big' : 'masonry' ),
		'thumb_only' => true,
		'show_no_image' => true,
		'post_info' => '<div class="post_details">'
							. '<h2 class="post_title"><a href="'.esc_url(get_permalink()).'">'. esc_html(get_the_title()) . '</a></h2>'
							. '<div class="post_description">'
								. (!empty($tiger_claw_components)
										? tiger_claw_show_post_meta(apply_filters('tiger_claw_filter_post_meta_args', array(
											'components' => $tiger_claw_components,
											'counters' => $tiger_claw_counters,
											'seo' => false,
											'echo' => false
											), $tiger_claw_blog_style[0], $tiger_claw_columns))
										: '')
								. '<div class="post_description_content">'
									. apply_filters('the_excerpt', get_the_excerpt())
								. '</div>'
								. '<a href="'.esc_url(get_permalink()).'" class="theme_button post_readmore"><span class="post_readmore_label">' . esc_html__('Learn more', 'tiger-claw') . '</span></a>'
							. '</div>'
						. '</div>'
	));
	?>
</article>