<?php
/**
 * The Classic template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

$tiger_claw_blog_style = explode('_', tiger_claw_get_theme_option('blog_style'));
$tiger_claw_columns = empty($tiger_claw_blog_style[1]) ? 1 : max(1, $tiger_claw_blog_style[1]);
$tiger_claw_expanded = !tiger_claw_sidebar_present() && tiger_claw_is_on(tiger_claw_get_theme_option('expand_content'));
$tiger_claw_post_format = get_post_format();
$tiger_claw_post_format = empty($tiger_claw_post_format) ? 'standard' : str_replace('post-format-', '', $tiger_claw_post_format);
$tiger_claw_animation = tiger_claw_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_chess post_layout_chess_'.esc_attr($tiger_claw_columns).' post_format_'.esc_attr($tiger_claw_post_format) ); ?>
	<?php echo (!tiger_claw_is_off($tiger_claw_animation) ? ' data-animation="'.esc_attr(tiger_claw_get_animation_classes($tiger_claw_animation)).'"' : ''); ?>>

	<?php
	// Add anchor
	if ($tiger_claw_columns == 1 && shortcode_exists('trx_sc_anchor')) {
		echo do_shortcode('[trx_sc_anchor id="post_'.esc_attr(get_the_ID()).'" title="'.esc_attr(get_the_title()).'"]');
	}

	// Sticky label
	if ( is_sticky() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

	// Featured image
	tiger_claw_show_post_featured( array(
											'class' => $tiger_claw_columns == 1 ? 'trx-stretch-height' : '',
											'show_no_image' => true,
											'thumb_bg' => true,
											'thumb_size' => tiger_claw_get_thumb_size(
																	strpos(tiger_claw_get_theme_option('body_style'), 'full')!==false
																		? ( $tiger_claw_columns > 1 ? 'huge' : 'original' )
																		: (	$tiger_claw_columns > 2 ? 'big' : 'huge')
																	)
											) 
										);

	?><div class="post_inner"><div class="post_inner_content"><?php 

		?><div class="post_header entry-header"><?php 
			do_action('tiger_claw_action_before_post_title'); 

			// Post title
			the_title( sprintf( '<h3 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
			
			do_action('tiger_claw_action_before_post_meta'); 

			// Post meta
			$tiger_claw_components = tiger_claw_is_inherit(tiger_claw_get_theme_option_from_meta('meta_parts')) 
										? 'categories,date'.($tiger_claw_columns < 3 ? ',counters' : '').($tiger_claw_columns == 1 ? ',edit' : '')
										: tiger_claw_array_get_keys_by_value(tiger_claw_get_theme_option('meta_parts'));
			$tiger_claw_counters = tiger_claw_is_inherit(tiger_claw_get_theme_option_from_meta('counters')) 
										? 'comments'
										: tiger_claw_array_get_keys_by_value(tiger_claw_get_theme_option('counters'));
			$tiger_claw_post_meta = empty($tiger_claw_components) 
										? '' 
										: tiger_claw_show_post_meta(apply_filters('tiger_claw_filter_post_meta_args', array(
												'components' => $tiger_claw_components,
												'counters' => $tiger_claw_counters,
												'seo' => false,
												'echo' => false
												), $tiger_claw_blog_style[0], $tiger_claw_columns)
											);
			tiger_claw_show_layout($tiger_claw_post_meta);
		?></div><!-- .entry-header -->
	
		<div class="post_content entry-content">
			<div class="post_content_inner">
				<?php
				$tiger_claw_show_learn_more = !in_array($tiger_claw_post_format, array('link', 'aside', 'status', 'quote'));
				if (has_excerpt()) {
					the_excerpt();
				} else if (strpos(get_the_content('!--more'), '!--more')!==false) {
					the_content( '' );
				} else if (in_array($tiger_claw_post_format, array('link', 'aside', 'status'))) {
					the_content();
				} else if ($tiger_claw_post_format == 'quote') {
					if (($quote = tiger_claw_get_tag(get_the_content(), '<blockquote>', '</blockquote>'))!='')
						tiger_claw_show_layout(wpautop($quote));
					else
						the_excerpt();
				} else if (substr(get_the_content(), 0, 1)!='[') {
					if($tiger_claw_columns > 1)
						echo mb_substr( strip_tags( get_the_excerpt() ), 0, 180 ).'...';
					else
						the_excerpt();

				}
				?>
			</div>
			<?php
			// Post meta
			if (in_array($tiger_claw_post_format, array('link', 'aside', 'status', 'quote'))) {
				tiger_claw_show_layout($tiger_claw_post_meta);
			}
			// More button
			if ( $tiger_claw_show_learn_more ) {
				?><p><a class="more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read more', 'tiger-claw'); ?></a></p><?php
			}
			?>
		</div><!-- .entry-content -->

	</div></div><!-- .post_inner -->

</article>