<?php
/**
 * The template to display the copyright info in the footer
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.10
 */

// Copyright area
$tiger_claw_footer_scheme =  tiger_claw_is_inherit(tiger_claw_get_theme_option('footer_scheme')) ? tiger_claw_get_theme_option('color_scheme') : tiger_claw_get_theme_option('footer_scheme');
$tiger_claw_copyright_scheme = tiger_claw_is_inherit(tiger_claw_get_theme_option('copyright_scheme')) ? $tiger_claw_footer_scheme : tiger_claw_get_theme_option('copyright_scheme');
?> 
<div class="footer_copyright_wrap scheme_<?php echo esc_attr($tiger_claw_copyright_scheme); ?>">
	<div class="footer_copyright_inner">
		<div class="content_wrap">
			<div class="copyright_text"><?php
				$tiger_claw_copyright = tiger_claw_prepare_macros(tiger_claw_get_theme_option('copyright'));
				if (!empty($tiger_claw_copyright)) {
					// Replace {{Y}} or {Y} with the current year
					$tiger_claw_copyright = str_replace(array('{{Y}}', '{Y}'), date('Y'), $tiger_claw_copyright);
					// Display copyright
					echo wp_kses_post(nl2br($tiger_claw_copyright));
				}
			?></div>
		</div>
	</div>
</div>
