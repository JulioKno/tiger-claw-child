<?php
/**
 * The template to display the site logo in the footer
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.10
 */

// Logo
if (tiger_claw_is_on(tiger_claw_get_theme_option('logo_in_footer'))) {
	$tiger_claw_logo_image = '';
	if (tiger_claw_get_retina_multiplier(2) > 1)
		$tiger_claw_logo_image = tiger_claw_get_theme_option( 'logo_footer_retina' );
	if (empty($tiger_claw_logo_image)) 
		$tiger_claw_logo_image = tiger_claw_get_theme_option( 'logo_footer' );
	$tiger_claw_logo_text   = get_bloginfo( 'name' );
	if (!empty($tiger_claw_logo_image) || !empty($tiger_claw_logo_text)) {
		?>
		<div class="footer_logo_wrap">
			<div class="footer_logo_inner">
				<?php
				if (!empty($tiger_claw_logo_image)) {
					$tiger_claw_attr = tiger_claw_getimagesize($tiger_claw_logo_image);
					echo '<a href="'.esc_url(home_url('/')).'"><img src="'.esc_url($tiger_claw_logo_image).'" class="logo_footer_image" alt="'.esc_attr__('logo', 'tiger-claw').'"'.(!empty($tiger_claw_attr[3]) ? sprintf(' %s', $tiger_claw_attr[3]) : '').'></a>' ;
				} else if (!empty($tiger_claw_logo_text)) {
					echo '<h1 class="logo_footer_text"><a href="'.esc_url(home_url('/')).'">' . esc_html($tiger_claw_logo_text) . '</a></h1>';
				}
				?>
			</div>
		</div>
		<?php
	}
}
?>