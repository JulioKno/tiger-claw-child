<?php
/**
 * The template to display the widgets area in the footer
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.10
 */

// Footer sidebar
$tiger_claw_footer_name = tiger_claw_get_theme_option('footer_widgets');
$tiger_claw_footer_present = !tiger_claw_is_off($tiger_claw_footer_name) && is_active_sidebar($tiger_claw_footer_name);
if ($tiger_claw_footer_present) { 
	tiger_claw_storage_set('current_sidebar', 'footer');
	$tiger_claw_footer_wide = tiger_claw_get_theme_option('footer_wide');
	ob_start();
	if ( is_active_sidebar($tiger_claw_footer_name) ) {
		dynamic_sidebar($tiger_claw_footer_name);
	}
	$tiger_claw_out = trim(ob_get_contents());
	ob_end_clean();
	if (!empty($tiger_claw_out)) {
		$tiger_claw_out = preg_replace("/<\\/aside>[\r\n\s]*<aside/", "</aside><aside", $tiger_claw_out);
		$tiger_claw_need_columns = true;	//or check: strpos($tiger_claw_out, 'columns_wrap')===false;
		if ($tiger_claw_need_columns) {
			$tiger_claw_columns = max(0, (int) tiger_claw_get_theme_option('footer_columns'));
			if ($tiger_claw_columns == 0) $tiger_claw_columns = min(4, max(1, substr_count($tiger_claw_out, '<aside ')));
			if ($tiger_claw_columns > 1)
				$tiger_claw_out = preg_replace("/class=\"widget /", "class=\"column-1_".esc_attr($tiger_claw_columns).' widget ', $tiger_claw_out);
			else
				$tiger_claw_need_columns = false;
		}
		?>
		<div class="footer_widgets_wrap widget_area<?php echo !empty($tiger_claw_footer_wide) ? ' footer_fullwidth' : ''; ?> sc_layouts_row  sc_layouts_row_type_normal">
			<div class="footer_widgets_inner widget_area_inner">
				<?php 
				if (!$tiger_claw_footer_wide) { 
					?><div class="content_wrap"><?php
				}
				if ($tiger_claw_need_columns) {
					?><div class="columns_wrap"><?php
				}
				do_action( 'tiger_claw_action_before_sidebar' );
				tiger_claw_show_layout($tiger_claw_out);
				do_action( 'tiger_claw_action_after_sidebar' );
				if ($tiger_claw_need_columns) {
					?></div><!-- /.columns_wrap --><?php
				}
				if (!$tiger_claw_footer_wide) {
					?></div><!-- /.content_wrap --><?php
				}
				?>
			</div><!-- /.footer_widgets_inner -->
		</div><!-- /.footer_widgets_wrap -->
		<?php
	}
}
?>