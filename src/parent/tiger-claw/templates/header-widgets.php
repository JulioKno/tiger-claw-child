<?php
/**
 * The template to display the widgets area in the header
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

// Header sidebar
$tiger_claw_header_name = tiger_claw_get_theme_option('header_widgets');
$tiger_claw_header_present = !tiger_claw_is_off($tiger_claw_header_name) && is_active_sidebar($tiger_claw_header_name);
if ($tiger_claw_header_present) { 
	tiger_claw_storage_set('current_sidebar', 'header');
	$tiger_claw_header_wide = tiger_claw_get_theme_option('header_wide');
	ob_start();
	if ( is_active_sidebar($tiger_claw_header_name) ) {
		dynamic_sidebar($tiger_claw_header_name);
	}
	$tiger_claw_widgets_output = ob_get_contents();
	ob_end_clean();
	if (!empty($tiger_claw_widgets_output)) {
		$tiger_claw_widgets_output = preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $tiger_claw_widgets_output);
		$tiger_claw_need_columns = strpos($tiger_claw_widgets_output, 'columns_wrap')===false;
		if ($tiger_claw_need_columns) {
			$tiger_claw_columns = max(0, (int) tiger_claw_get_theme_option('header_columns'));
			if ($tiger_claw_columns == 0) $tiger_claw_columns = min(6, max(1, substr_count($tiger_claw_widgets_output, '<aside ')));
			if ($tiger_claw_columns > 1)
				$tiger_claw_widgets_output = preg_replace("/class=\"widget /", "class=\"column-1_".esc_attr($tiger_claw_columns).' widget ', $tiger_claw_widgets_output);
			else
				$tiger_claw_need_columns = false;
		}
		?>
		<div class="header_widgets_wrap widget_area<?php echo !empty($tiger_claw_header_wide) ? ' header_fullwidth' : ' header_boxed'; ?>">
			<div class="header_widgets_inner widget_area_inner">
				<?php 
				if (!$tiger_claw_header_wide) { 
					?><div class="content_wrap"><?php
				}
				if ($tiger_claw_need_columns) {
					?><div class="columns_wrap"><?php
				}
				do_action( 'tiger_claw_action_before_sidebar' );
				tiger_claw_show_layout($tiger_claw_widgets_output);
				do_action( 'tiger_claw_action_after_sidebar' );
				if ($tiger_claw_need_columns) {
					?></div>	<!-- /.columns_wrap --><?php
				}
				if (!$tiger_claw_header_wide) {
					?></div>	<!-- /.content_wrap --><?php
				}
				?>
			</div>	<!-- /.header_widgets_inner -->
		</div>	<!-- /.header_widgets_wrap -->
		<?php
	}
}
?>