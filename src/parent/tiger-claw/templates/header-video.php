<?php
/**
 * The template to display the background video in the header
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.14
 */
$tiger_claw_header_video = tiger_claw_get_header_video();
$tiger_claw_embed_video = '';
if (!empty($tiger_claw_header_video) && !tiger_claw_is_from_uploads($tiger_claw_header_video)) {
	if (tiger_claw_is_youtube_url($tiger_claw_header_video) && preg_match('/[=\/]([^=\/]*)$/', $tiger_claw_header_video, $matches) && !empty($matches[1])) {
		?><div id="background_video" data-youtube-code="<?php echo esc_attr($matches[1]); ?>"></div><?php
	} else {
		global $wp_embed;
		if (false && is_object($wp_embed)) {
			$tiger_claw_embed_video = do_shortcode($wp_embed->run_shortcode( '[embed]' . trim($tiger_claw_header_video) . '[/embed]' ));
			$tiger_claw_embed_video = tiger_claw_make_video_autoplay($tiger_claw_embed_video);
		} else {
			$tiger_claw_header_video = str_replace('/watch?v=', '/embed/', $tiger_claw_header_video);
			$tiger_claw_header_video = tiger_claw_add_to_url($tiger_claw_header_video, array(
				'feature' => 'oembed',
				'controls' => 0,
				'autoplay' => 1,
				'showinfo' => 0,
				'modestbranding' => 1,
				'wmode' => 'transparent',
				'enablejsapi' => 1,
				'origin' => home_url(),
				'widgetid' => 1
			));
			$tiger_claw_embed_video = '<iframe src="' . esc_url($tiger_claw_header_video) . '" width="1170" height="658" allowfullscreen="0"></iframe>';
		}
		?><div id="background_video"><?php tiger_claw_show_layout($tiger_claw_embed_video); ?></div><?php
	}
}
?>