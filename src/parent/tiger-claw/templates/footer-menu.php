<?php
/**
 * The template to display menu in the footer
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.10
 */

// Footer menu
$tiger_claw_menu_footer = tiger_claw_get_nav_menu(array(
											'location' => 'menu_footer',
											'class' => 'sc_layouts_menu sc_layouts_menu_default'
											));
if (!empty($tiger_claw_menu_footer)) {
	?>
	<div class="footer_menu_wrap">
		<div class="footer_menu_inner">
			<?php tiger_claw_show_layout($tiger_claw_menu_footer); ?>
		</div>
	</div>
	<?php
}
?>