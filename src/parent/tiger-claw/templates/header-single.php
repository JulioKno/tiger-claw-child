<?php
/**
 * The template to display the featured image in the single post
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

if ( get_query_var('tiger_claw_header_image')=='' && is_singular() && has_post_thumbnail() && in_array(get_post_type(), array('post', 'page')) )  {
	$tiger_claw_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
	if (!empty($tiger_claw_src[0])) {
		tiger_claw_sc_layouts_showed('featured', true);
		?><div class="sc_layouts_featured with_image <?php echo esc_attr(tiger_claw_add_inline_css_class('background-image:url('.esc_url($tiger_claw_src[0]).');')); ?>"></div><?php
	}
}
?>