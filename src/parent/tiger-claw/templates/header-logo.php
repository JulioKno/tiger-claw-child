<?php
/**
 * The template to display the logo or the site name and the slogan in the Header
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

$tiger_claw_args = get_query_var('tiger_claw_logo_args');

// Site logo
$tiger_claw_logo_image  = tiger_claw_get_logo_image(isset($tiger_claw_args['type']) ? $tiger_claw_args['type'] : '');
$tiger_claw_logo_text   = tiger_claw_is_on(tiger_claw_get_theme_option('logo_text')) ? get_bloginfo( 'name' ) : '';
$tiger_claw_logo_slogan = get_bloginfo( 'description', 'display' );
if (!empty($tiger_claw_logo_image) || !empty($tiger_claw_logo_text)) {
	?><a class="sc_layouts_logo" href="<?php echo is_front_page() ? '#' : esc_url(home_url('/')); ?>"><?php
		if (!empty($tiger_claw_logo_image)) {
			$tiger_claw_attr = tiger_claw_getimagesize($tiger_claw_logo_image);
			echo '<img src="'.esc_url($tiger_claw_logo_image).'" alt="'.esc_attr__('logo', 'tiger-claw').'"'.(!empty($tiger_claw_attr[3]) ? sprintf(' %s', $tiger_claw_attr[3]) : '').'>' ;
		} else {
			tiger_claw_show_layout(tiger_claw_prepare_macros($tiger_claw_logo_text), '<span class="logo_text">', '</span>');
			tiger_claw_show_layout(tiger_claw_prepare_macros($tiger_claw_logo_slogan), '<span class="logo_slogan">', '</span>');
		}
	?></a><?php
}
?>