<?php
/**
 * The template to display default site header
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

$tiger_claw_header_css = $tiger_claw_header_image = '';
$tiger_claw_header_video = tiger_claw_get_header_video();
if (true || empty($tiger_claw_header_video)) {
	$tiger_claw_header_image = get_header_image();
	if (tiger_claw_is_on(tiger_claw_get_theme_option('header_image_override')) && apply_filters('tiger_claw_filter_allow_override_header_image', true)) {
		if (is_category()) {
			if (($tiger_claw_cat_img = tiger_claw_get_category_image()) != '')
				$tiger_claw_header_image = $tiger_claw_cat_img;
		} else if (is_singular() || tiger_claw_storage_isset('blog_archive')) {
			if (has_post_thumbnail()) {
				$tiger_claw_header_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				if (is_array($tiger_claw_header_image)) $tiger_claw_header_image = $tiger_claw_header_image[0];
			} else
				$tiger_claw_header_image = '';
		}
	}
}

?><header class="top_panel style_image_top top_panel_default<?php
					echo !empty($tiger_claw_header_image) || !empty($tiger_claw_header_video) ? ' with_bg_image' : ' without_bg_image';
					if ($tiger_claw_header_video!='') echo ' with_bg_video';
					if ($tiger_claw_header_image!='') echo ' '.esc_attr(tiger_claw_add_inline_css_class('background-image: url('.esc_url($tiger_claw_header_image).');'));
					if (is_single() && has_post_thumbnail()) echo ' with_featured_image';
					if (tiger_claw_is_on(tiger_claw_get_theme_option('header_fullheight'))) echo ' header_fullheight trx-stretch-height';
					?> scheme_<?php echo esc_attr(tiger_claw_is_inherit(tiger_claw_get_theme_option('header_scheme')) 
													? tiger_claw_get_theme_option('color_scheme') 
													: tiger_claw_get_theme_option('header_scheme'));
					?>"><?php

	// Background video
	if (!empty($tiger_claw_header_video)) {
		get_template_part( 'templates/header-video' );
	}

	$top_text_1 = tiger_claw_get_theme_option('top_text_1');
	$top_text_2 = tiger_claw_get_theme_option('top_text_2');
	$top_text_3 = tiger_claw_get_theme_option('top_text_3');

	if(!empty($top_text_1) || !empty($top_text_2) || !empty($top_text_3)) {	?>

		<div class="top_panel_top_area sc_layouts_row sc_layouts_row_type_narrow
			scheme_<?php echo esc_attr(tiger_claw_is_inherit(tiger_claw_get_theme_option('menu_scheme'))
			? (tiger_claw_is_inherit(tiger_claw_get_theme_option('header_scheme'))
				? tiger_claw_get_theme_option('color_scheme')
				: tiger_claw_get_theme_option('header_scheme'))
			: tiger_claw_get_theme_option('menu_scheme')); ?>">
			<div class="content_wrap_top">
				<?php if(!empty($top_text_1)) { ?>
				<div class="sc_layouts_item">
					<?php echo esc_html($top_text_1); ?>
				</div>
				<?php } ?>
				<?php if(!empty($top_text_2)) { ?>
					<div class="sc_layouts_item">
						<?php echo esc_html($top_text_2); ?>
					</div>
				<?php } ?>
				<?php if(!empty($top_text_3)) { ?>
					<div class="sc_layouts_item">
						<?php echo esc_html($top_text_3); ?>
					</div>
				<?php } ?>
			</div>
		</div>

	<?php
	}

	// Main menu
	if (tiger_claw_get_theme_option("menu_style") == 'top') {
		get_template_part( 'templates/header-navi' );
	}

	// Page title and breadcrumbs area
	get_template_part( 'templates/header-title');

	// Header widgets area
	get_template_part( 'templates/header-widgets' );



?></header>