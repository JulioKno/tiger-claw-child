<?php
/**
 * The template to display default site footer
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.10
 */

$tiger_claw_footer_scheme =  tiger_claw_is_inherit(tiger_claw_get_theme_option('footer_scheme')) ? tiger_claw_get_theme_option('color_scheme') : tiger_claw_get_theme_option('footer_scheme');
$tiger_claw_footer_id = str_replace('footer-custom-', '', tiger_claw_get_theme_option("footer_style"));
$tiger_claw_footer_meta = get_post_meta($tiger_claw_footer_id, 'trx_addons_options', true);
?>
<footer class="footer_wrap footer_custom footer_custom_<?php echo esc_attr($tiger_claw_footer_id); 
						?> footer_custom_<?php echo esc_attr(sanitize_title(get_the_title($tiger_claw_footer_id))); 
						if (!empty($tiger_claw_footer_meta['margin']) != '') 
							echo ' '.esc_attr(tiger_claw_add_inline_css_class('margin-top: '.esc_attr(tiger_claw_prepare_css_value($tiger_claw_footer_meta['margin'])).';'));
						?> scheme_<?php echo esc_attr($tiger_claw_footer_scheme); 
						?>">
	<?php
    // Custom footer's layout
    do_action('tiger_claw_action_show_layout', $tiger_claw_footer_id);
	?>
</footer><!-- /.footer_wrap -->
