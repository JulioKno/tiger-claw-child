<?php
/**
 * The template to display posts in widgets and/or in the search results
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

$tiger_claw_post_id    = get_the_ID();
$tiger_claw_post_date  = tiger_claw_get_date();
$tiger_claw_post_title = get_the_title();
$tiger_claw_post_link  = get_permalink();
$tiger_claw_post_author_id   = get_the_author_meta('ID');
$tiger_claw_post_author_name = get_the_author_meta('display_name');
$tiger_claw_post_author_url  = get_author_posts_url($tiger_claw_post_author_id, '');

$tiger_claw_args = get_query_var('tiger_claw_args_widgets_posts');
$tiger_claw_show_date = isset($tiger_claw_args['show_date']) ? (int) $tiger_claw_args['show_date'] : 1;
$tiger_claw_show_image = isset($tiger_claw_args['show_image']) ? (int) $tiger_claw_args['show_image'] : 1;
$tiger_claw_show_author = isset($tiger_claw_args['show_author']) ? (int) $tiger_claw_args['show_author'] : 1;
$tiger_claw_show_counters = isset($tiger_claw_args['show_counters']) ? (int) $tiger_claw_args['show_counters'] : 1;
$tiger_claw_show_categories = isset($tiger_claw_args['show_categories']) ? (int) $tiger_claw_args['show_categories'] : 1;

$tiger_claw_output = tiger_claw_storage_get('tiger_claw_output_widgets_posts');

$tiger_claw_post_counters_output = '';
if ( $tiger_claw_show_counters ) {
	$tiger_claw_post_counters_output = '<span class="post_info_item post_info_counters">'
								. tiger_claw_get_post_counters('comments')
							. '</span>';
}


$tiger_claw_output .= '<article class="post_item with_thumb">';

if ($tiger_claw_show_image) {
	$tiger_claw_post_thumb = get_the_post_thumbnail($tiger_claw_post_id, tiger_claw_get_thumb_size('tiny'), array(
		'alt' => get_the_title()
	));
	if ($tiger_claw_post_thumb) $tiger_claw_output .= '<div class="post_thumb">' . ($tiger_claw_post_link ? '<a href="' . esc_url($tiger_claw_post_link) . '">' : '') . ($tiger_claw_post_thumb) . ($tiger_claw_post_link ? '</a>' : '') . '</div>';
}

$tiger_claw_output .= '<div class="post_content">'
			. ($tiger_claw_show_categories 
					? '<div class="post_categories">'
						. tiger_claw_get_post_categories()
						. $tiger_claw_post_counters_output
						. '</div>' 
					: '')
			. '<h6 class="post_title">' . ($tiger_claw_post_link ? '<a href="' . esc_url($tiger_claw_post_link) . '">' : '') . ($tiger_claw_post_title) . ($tiger_claw_post_link ? '</a>' : '') . '</h6>'
			. apply_filters('tiger_claw_filter_get_post_info', 
								'<div class="post_info">'
									. ($tiger_claw_show_date 
										? '<span class="post_info_item post_info_posted">'
											. ($tiger_claw_post_link ? '<a href="' . esc_url($tiger_claw_post_link) . '" class="post_info_date">' : '') 
											. esc_html($tiger_claw_post_date) 
											. ($tiger_claw_post_link ? '</a>' : '')
											. '</span>'
										: '')
									. ($tiger_claw_show_author 
										? '<span class="post_info_item post_info_posted_by">' 
											. esc_html__('by', 'tiger-claw') . ' ' 
											. ($tiger_claw_post_link ? '<a href="' . esc_url($tiger_claw_post_author_url) . '" class="post_info_author">' : '') 
											. esc_html($tiger_claw_post_author_name) 
											. ($tiger_claw_post_link ? '</a>' : '') 
											. '</span>'
										: '')
									. (!$tiger_claw_show_categories && $tiger_claw_post_counters_output
										? $tiger_claw_post_counters_output
										: '')
								. '</div>')
		. '</div>'
	. '</article>';
tiger_claw_storage_set('tiger_claw_output_widgets_posts', $tiger_claw_output);
?>