<?php
/**
 * The template to display custom header from the ThemeREX Addons Layouts
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.06
 */

$tiger_claw_header_css = $tiger_claw_header_image = '';
$tiger_claw_header_video = tiger_claw_get_header_video();
if (true || empty($tiger_claw_header_video)) {
	$tiger_claw_header_image = get_header_image();
	if (tiger_claw_is_on(tiger_claw_get_theme_option('header_image_override')) && apply_filters('tiger_claw_filter_allow_override_header_image', true)) {
		if (is_category()) {
			if (($tiger_claw_cat_img = tiger_claw_get_category_image()) != '')
				$tiger_claw_header_image = $tiger_claw_cat_img;
		} else if (is_singular() || tiger_claw_storage_isset('blog_archive')) {
			if (has_post_thumbnail()) {
				$tiger_claw_header_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				if (is_array($tiger_claw_header_image)) $tiger_claw_header_image = $tiger_claw_header_image[0];
			} else
				$tiger_claw_header_image = '';
		}
	}
}

$tiger_claw_header_id = str_replace('header-custom-', '', tiger_claw_get_theme_option("header_style"));

$tiger_claw_header_meta = get_post_meta($tiger_claw_header_id, 'trx_addons_options', true);

?><header class="top_panel top_panel_custom top_panel_custom_<?php echo esc_attr($tiger_claw_header_id); 
						?> top_panel_custom_<?php echo esc_attr(sanitize_title(get_the_title($tiger_claw_header_id)));
						echo !empty($tiger_claw_header_image) || !empty($tiger_claw_header_video) 
							? ' with_bg_image' 
							: ' without_bg_image';
						if ($tiger_claw_header_video!='') 
							echo ' with_bg_video';
						if ($tiger_claw_header_image!='') 
							echo ' '.esc_attr(tiger_claw_add_inline_css_class('background-image: url('.esc_url($tiger_claw_header_image).');'));
						if (!empty($tiger_claw_header_meta['margin']) != '') 
							echo ' '.esc_attr(tiger_claw_add_inline_css_class('margin-bottom: '.esc_attr(tiger_claw_prepare_css_value($tiger_claw_header_meta['margin'])).';'));
						if (is_single() && has_post_thumbnail()) 
							echo ' with_featured_image';
						if (tiger_claw_is_on(tiger_claw_get_theme_option('header_fullheight'))) 
							echo ' header_fullheight trx-stretch-height';
						?> scheme_<?php echo esc_attr(tiger_claw_is_inherit(tiger_claw_get_theme_option('header_scheme')) 
														? tiger_claw_get_theme_option('color_scheme') 
														: tiger_claw_get_theme_option('header_scheme'));
						?>"><?php

	// Background video
	if (!empty($tiger_claw_header_video)) {
		get_template_part( 'templates/header-video' );
	}
		
	// Custom header's layout
	do_action('tiger_claw_action_show_layout', $tiger_claw_header_id);

	// Header widgets area
	get_template_part( 'templates/header-widgets' );
		
?></header>