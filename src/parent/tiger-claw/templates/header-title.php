<?php
/**
 * The template to display the page title and breadcrumbs
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

// Page (category, tag, archive, author) title

if ( tiger_claw_need_page_title() ) {
	tiger_claw_sc_layouts_showed('title', true);
	tiger_claw_sc_layouts_showed('postmeta', false);
	?>
	<div class="top_panel_title sc_layouts_row sc_layouts_row_type_normal">
		<div class="content_wrap">
			<div class="sc_layouts_column sc_layouts_column_align_center">
				<div class="sc_layouts_item">
					<div class="sc_layouts_title sc_align_center">
						<?php
						// Post meta on the single post
						if ( false && is_single() )  {
							?><div class="sc_layouts_title_meta"><?php
								tiger_claw_show_post_meta(apply_filters('tiger_claw_filter_post_meta_args', array(
									'components' => 'categories,date,counters,edit',
									'counters' => 'views,comments,likes',
									'seo' => true
									), 'header', 1)
								);
							?></div><?php
						}
						
						// Blog/Post title
						?><div class="sc_layouts_title_title"><?php
							$tiger_claw_blog_title = tiger_claw_get_blog_title();
							$tiger_claw_blog_title_text = $tiger_claw_blog_title_class = $tiger_claw_blog_title_link = $tiger_claw_blog_title_link_text = '';
							if (is_array($tiger_claw_blog_title)) {
								$tiger_claw_blog_title_text = $tiger_claw_blog_title['text'];
								$tiger_claw_blog_title_class = !empty($tiger_claw_blog_title['class']) ? ' '.$tiger_claw_blog_title['class'] : '';
								$tiger_claw_blog_title_link = !empty($tiger_claw_blog_title['link']) ? $tiger_claw_blog_title['link'] : '';
								$tiger_claw_blog_title_link_text = !empty($tiger_claw_blog_title['link_text']) ? $tiger_claw_blog_title['link_text'] : '';
							} else
								$tiger_claw_blog_title_text = $tiger_claw_blog_title;
							?>
							<h1 class="sc_layouts_title_caption<?php echo esc_attr($tiger_claw_blog_title_class); ?>"><?php
								$tiger_claw_top_icon = tiger_claw_get_category_icon();
								if (!empty($tiger_claw_top_icon)) {
									$tiger_claw_attr = tiger_claw_getimagesize($tiger_claw_top_icon);
									?><img src="<?php echo esc_url($tiger_claw_top_icon); ?>" alt="'.esc_attr__('icon', 'tiger-claw').'" <?php if (!empty($tiger_claw_attr[3])) tiger_claw_show_layout($tiger_claw_attr[3]);?>><?php
								}
								echo wp_kses_data($tiger_claw_blog_title_text);
							?></h1>
							<?php
							if (!empty($tiger_claw_blog_title_link) && !empty($tiger_claw_blog_title_link_text)) {
								?><a href="<?php echo esc_url($tiger_claw_blog_title_link); ?>" class="theme_button theme_button_small sc_layouts_title_link"><?php echo esc_html($tiger_claw_blog_title_link_text); ?></a><?php
							}
							
							// Category/Tag description
							if ( is_category() || is_tag() || is_tax() ) 
								the_archive_description( '<div class="sc_layouts_title_description">', '</div>' );
		
						?></div><?php
	
						// Breadcrumbs
						?><div class="sc_layouts_title_breadcrumbs"><?php
							do_action( 'tiger_claw_action_breadcrumbs');
						?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>