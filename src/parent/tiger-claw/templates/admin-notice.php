<?php
/**
 * The template to display Admin notices
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.1
 */
?>
<div class="update-nag" id="tiger_claw_admin_notice">
	<h3 class="tiger_claw_notice_title"><?php echo sprintf(esc_html__('Welcome to %s', 'tiger-claw'), wp_get_theme()->name); ?></h3>
	<?php
	if (!tiger_claw_exists_trx_addons()) {
		?><p><?php echo wp_kses_data(__('<b>Attention!</b> Plugin "ThemeREX Addons is required! Please, install and activate it!', 'tiger-claw')); ?></p><?php
	}
	?><p><?php
		if (tiger_claw_get_value_gp('page')!='tgmpa-install-plugins') {
			?>
			<a href="<?php echo esc_url(admin_url().'themes.php?page=tgmpa-install-plugins'); ?>" class="button-primary"><i class="dashicons dashicons-admin-plugins"></i> <?php esc_html_e('Install plugins', 'tiger-claw'); ?></a>
			<?php
		}
		if (function_exists('tiger_claw_exists_trx_addons') && tiger_claw_exists_trx_addons()) {
			?>
			<a href="<?php echo esc_url(admin_url().'themes.php?page=trx_importer'); ?>" class="button-primary"><i class="dashicons dashicons-download"></i> <?php esc_html_e('One Click Demo Data', 'tiger-claw'); ?></a>
			<?php
		}
		?>
        <a href="<?php echo esc_url(admin_url().'customize.php'); ?>" class="button-primary"><i class="dashicons dashicons-admin-appearance"></i> <?php esc_html_e('Theme Customizer', 'tiger-claw'); ?></a>
        <a href="#" class="button tiger_claw_hide_notice"><i class="dashicons dashicons-dismiss"></i> <?php esc_html_e('Hide Notice', 'tiger-claw'); ?></a>
	</p>
</div>