<?php
/**
 * The template to display the socials in the footer
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0.10
 */


// Socials
if ( tiger_claw_is_on(tiger_claw_get_theme_option('socials_in_footer')) && ($tiger_claw_output = tiger_claw_get_socials_links()) != '') {
	?>
	<div class="footer_socials_wrap socials_wrap">
		<div class="footer_socials_inner">
			<?php tiger_claw_show_layout($tiger_claw_output); ?>
		</div>
	</div>
	<?php
}
?>