<?php
/**
 * The Sticky template to display the sticky posts
 *
 * Used for index/archive
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

$tiger_claw_columns = max(1, min(3, count(get_option( 'sticky_posts' ))));
$tiger_claw_post_format = get_post_format();
$tiger_claw_post_format = empty($tiger_claw_post_format) ? 'standard' : str_replace('post-format-', '', $tiger_claw_post_format);
$tiger_claw_animation = tiger_claw_get_theme_option('blog_animation');

?><div class="column-1_<?php echo esc_attr($tiger_claw_columns); ?>"><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_sticky post_format_'.esc_attr($tiger_claw_post_format) ); ?>
	<?php echo (!tiger_claw_is_off($tiger_claw_animation) ? ' data-animation="'.esc_attr(tiger_claw_get_animation_classes($tiger_claw_animation)).'"' : ''); ?>
	>

	<?php
	if ( is_sticky() && is_home() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

	// Featured image
	tiger_claw_show_post_featured(array(
		'thumb_size' => tiger_claw_get_thumb_size($tiger_claw_columns==1 ? 'big' : ($tiger_claw_columns==2 ? 'med' : 'avatar'))
	));

	if ( !in_array($tiger_claw_post_format, array('link', 'aside', 'status', 'quote')) ) {
		?>
		<div class="post_header entry-header">
			<?php
			// Post title
			the_title( sprintf( '<h6 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h6>' );
			// Post meta
			tiger_claw_show_post_meta(apply_filters('tiger_claw_filter_post_meta_args', array(), 'sticky', $tiger_claw_columns));
			?>
		</div><!-- .entry-header -->
		<?php
	}
	?>
</article></div>