<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

$tiger_claw_sidebar_position = tiger_claw_get_theme_option('sidebar_position');
if (tiger_claw_sidebar_present()) {
	ob_start();
	$tiger_claw_sidebar_name = tiger_claw_get_theme_option('sidebar_widgets');
	tiger_claw_storage_set('current_sidebar', 'sidebar');
	if ( is_active_sidebar($tiger_claw_sidebar_name) ) {
		dynamic_sidebar($tiger_claw_sidebar_name);
	}
	$tiger_claw_out = trim(ob_get_contents());
	ob_end_clean();
	if (!empty($tiger_claw_out)) {
		?>
		<div class="sidebar <?php echo esc_attr($tiger_claw_sidebar_position); ?> widget_area<?php if (!tiger_claw_is_inherit(tiger_claw_get_theme_option('sidebar_scheme'))) echo ' scheme_'.esc_attr(tiger_claw_get_theme_option('sidebar_scheme')); ?>" role="complementary">
			<div class="sidebar_inner">
				<?php
				do_action( 'tiger_claw_action_before_sidebar' );
				tiger_claw_show_layout(preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $tiger_claw_out));
				do_action( 'tiger_claw_action_after_sidebar' );
				?>
			</div><!-- /.sidebar_inner -->
		</div><!-- /.sidebar -->
		<?php
	}
}
?>