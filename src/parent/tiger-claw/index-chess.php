<?php
/**
 * The template for homepage posts with "Chess" style
 *
 * @package WordPress
 * @subpackage TIGER_CLAW
 * @since TIGER_CLAW 1.0
 */

tiger_claw_storage_set('blog_archive', true);

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$tiger_claw_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$tiger_claw_sticky_out = tiger_claw_get_theme_option('sticky_style')=='columns' 
							&& is_array($tiger_claw_stickies) && count($tiger_claw_stickies) > 0 && get_query_var( 'paged' ) < 1;
	if ($tiger_claw_sticky_out) {
		?><div class="sticky_wrap columns_wrap"><?php	
	}
	if (!$tiger_claw_sticky_out) {
		?><div class="chess_wrap posts_container"><?php
	}
	while ( have_posts() ) { the_post(); 
		if ($tiger_claw_sticky_out && !is_sticky()) {
			$tiger_claw_sticky_out = false;
			?></div><div class="chess_wrap posts_container"><?php
		}
		get_template_part( 'content', $tiger_claw_sticky_out && is_sticky() ? 'sticky' :'chess' );
	}
	
	?></div><?php

	tiger_claw_show_pagination();

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>