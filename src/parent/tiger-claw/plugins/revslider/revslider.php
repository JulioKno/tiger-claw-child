<?php
/* Revolution Slider support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('tiger_claw_revslider_theme_setup9')) {
	add_action( 'after_setup_theme', 'tiger_claw_revslider_theme_setup9', 9 );
	function tiger_claw_revslider_theme_setup9() {
		if (tiger_claw_exists_revslider()) {
			add_action( 'wp_enqueue_scripts', 					'tiger_claw_revslider_frontend_scripts', 1100 );
			add_filter( 'tiger_claw_filter_merge_styles',			'tiger_claw_revslider_merge_styles' );
		}
		if (is_admin()) {
			add_filter( 'tiger_claw_filter_tgmpa_required_plugins','tiger_claw_revslider_tgmpa_required_plugins' );
		}
	}
}

// Check if RevSlider installed and activated
if ( !function_exists( 'tiger_claw_exists_revslider' ) ) {
	function tiger_claw_exists_revslider() {
		return function_exists('rev_slider_shortcode');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'tiger_claw_revslider_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('tiger_claw_filter_tgmpa_required_plugins',	'tiger_claw_revslider_tgmpa_required_plugins');
	function tiger_claw_revslider_tgmpa_required_plugins($list=array()) {
		if (in_array('revslider', tiger_claw_storage_get('required_plugins'))) {
			$path = tiger_claw_get_file_dir('plugins/revslider/revslider.zip');
			$list[] = array(
					'name' 		=> esc_html__('Revolution Slider', 'tiger-claw'),
					'slug' 		=> 'revslider',
					'source'	=> !empty($path) ? $path : 'upload://revslider.zip',
					'required' 	=> false
			);
		}
		return $list;
	}
}
	
// Enqueue custom styles
if ( !function_exists( 'tiger_claw_revslider_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'tiger_claw_revslider_frontend_scripts', 1100 );
	function tiger_claw_revslider_frontend_scripts() {
		if (tiger_claw_is_on(tiger_claw_get_theme_option('debug_mode')) && tiger_claw_get_file_dir('plugins/revslider/revslider.css')!='')
			wp_enqueue_style( 'revslider',  tiger_claw_get_file_url('plugins/revslider/revslider.css'), array(), null );
	}
}
	
// Merge custom styles
if ( !function_exists( 'tiger_claw_revslider_merge_styles' ) ) {
	//Handler of the add_filter('tiger_claw_filter_merge_styles', 'tiger_claw_revslider_merge_styles');
	function tiger_claw_revslider_merge_styles($list) {
		$list[] = 'plugins/revslider/revslider.css';
		return $list;
	}
}
?>