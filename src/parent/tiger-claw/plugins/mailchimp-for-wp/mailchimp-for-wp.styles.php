<?php
// Add plugin-specific colors and fonts to the custom CSS
if (!function_exists('tiger_claw_mailchimp_get_css')) {
	add_filter('tiger_claw_filter_get_css', 'tiger_claw_mailchimp_get_css', 10, 4);
	function tiger_claw_mailchimp_get_css($css, $colors, $fonts, $scheme='') {
		
		if (isset($css['fonts']) && $fonts) {
			$css['fonts'] .= <<<CSS

CSS;
		
			
			$rad = tiger_claw_get_border_radius();
			$css['fonts'] .= <<<CSS

CSS;
		}

		
		if (isset($css['colors']) && $colors) {
			$css['colors'] .= <<<CSS

.mc4wp-form input[type="email"] {
	background-color: {$colors['input_bg_color']};
	border-color: {$colors['input_bg_color']};
	color: {$colors['alter_text']};
}
.mc4wp-form input[type="email"]:focus {
	background-color: {$colors['input_bg_color']};
	border-color: {$colors['input_bg_color']};
	color: {$colors['inverse_dark']};
}

.mc4wp-form input[type="email"]::-webkit-input-placeholder { color: {$colors['alter_text']}; opacity: 1; }
.mc4wp-form input[type="email"]::-moz-placeholder          { color: {$colors['alter_text']}; opacity: 1; }
.mc4wp-form input[type="email"]:-ms-input-placeholder      { color: {$colors['alter_text']}; opacity: 1; }

.mc4wp-form input[type="email"]:focus::-webkit-input-placeholder { color: {$colors['inverse_dark']}; opacity: 1; }
.mc4wp-form input[type="email"]:focus::-moz-placeholder          { color: {$colors['inverse_dark']}; opacity: 1; }
.mc4wp-form input[type="email"]:focus:-ms-input-placeholder      { color: {$colors['inverse_dark']}; opacity: 1; }


footer .mc4wp-form-fields input[type="submit"] {
	background-color: {$colors['text_link']};
	color: {$colors['inverse_link']};
}
footer .mc4wp-form-fields input[type="submit"]:hover {
	background-color: #999999;
	color: {$colors['inverse_link']};
}


.mc4wp-form .mc4wp-alert {
	background-color: {$colors['text_link']};
	border-color: {$colors['text_hover']};
	color: {$colors['inverse_link']};
}
CSS;
		}

		return $css;
	}
}
?>