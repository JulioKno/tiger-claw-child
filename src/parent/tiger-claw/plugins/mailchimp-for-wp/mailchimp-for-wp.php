<?php
/* Mail Chimp support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('tiger_claw_mailchimp_theme_setup9')) {
	add_action( 'after_setup_theme', 'tiger_claw_mailchimp_theme_setup9', 9 );
	function tiger_claw_mailchimp_theme_setup9() {
		if (tiger_claw_exists_mailchimp()) {
			add_action( 'wp_enqueue_scripts',							'tiger_claw_mailchimp_frontend_scripts', 1100 );
			add_filter( 'tiger_claw_filter_merge_styles',					'tiger_claw_mailchimp_merge_styles');
		}
		if (is_admin()) {
			add_filter( 'tiger_claw_filter_tgmpa_required_plugins',		'tiger_claw_mailchimp_tgmpa_required_plugins' );
		}
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'tiger_claw_exists_mailchimp' ) ) {
	function tiger_claw_exists_mailchimp() {
		return function_exists('__mc4wp_load_plugin') || defined('MC4WP_VERSION');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'tiger_claw_mailchimp_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('tiger_claw_filter_tgmpa_required_plugins',	'tiger_claw_mailchimp_tgmpa_required_plugins');
	function tiger_claw_mailchimp_tgmpa_required_plugins($list=array()) {
		if (in_array('mailchimp-for-wp', tiger_claw_storage_get('required_plugins')))
			$list[] = array(
				'name' 		=> esc_html__('MailChimp for WP', 'tiger-claw'),
				'slug' 		=> 'mailchimp-for-wp',
				'required' 	=> false
			);
		return $list;
	}
}



// Custom styles and scripts
//------------------------------------------------------------------------

// Enqueue custom styles
if ( !function_exists( 'tiger_claw_mailchimp_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'tiger_claw_mailchimp_frontend_scripts', 1100 );
	function tiger_claw_mailchimp_frontend_scripts() {
		if (tiger_claw_exists_mailchimp()) {
			if (tiger_claw_is_on(tiger_claw_get_theme_option('debug_mode')) && tiger_claw_get_file_dir('plugins/mailchimp-for-wp/mailchimp-for-wp.css')!='')
				wp_enqueue_style( 'mailchimp-for-wp',  tiger_claw_get_file_url('plugins/mailchimp-for-wp/mailchimp-for-wp.css'), array(), null );
		}
	}
}
	
// Merge custom styles
if ( !function_exists( 'tiger_claw_mailchimp_merge_styles' ) ) {
	//Handler of the add_filter( 'tiger_claw_filter_merge_styles', 'tiger_claw_mailchimp_merge_styles');
	function tiger_claw_mailchimp_merge_styles($list) {
		$list[] = 'plugins/mailchimp-for-wp/mailchimp-for-wp.css';
		return $list;
	}
}


// Add plugin-specific colors and fonts to the custom CSS
if (tiger_claw_exists_mailchimp()) { require_once TIGER_CLAW_THEME_DIR . 'plugins/mailchimp-for-wp/mailchimp-for-wp.styles.php'; }
?>