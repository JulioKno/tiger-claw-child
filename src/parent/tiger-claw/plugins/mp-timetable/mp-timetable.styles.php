<?php
// Add plugin-specific colors and fonts to the custom CSS
if ( !function_exists( 'tiger_claw_mptt_get_css' ) ) {
	add_filter( 'tiger_claw_filter_get_css', 'tiger_claw_mptt_get_css', 10, 4 );
	function tiger_claw_mptt_get_css($css, $colors, $fonts, $scheme='') {
		if (isset($css['fonts']) && $fonts) {
			$css['fonts'] .= <<<CSS

CSS;
		}

		if (isset($css['colors']) && $colors) {
			$css['colors'] .= <<<CSS

CSS;
		}
		
		return $css;
	}
}
?>