<?php
/* MP Timetable support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('tiger_claw_mptt_theme_setup9')) {
	add_action( 'after_setup_theme', 'tiger_claw_mptt_theme_setup9', 9 );
	function tiger_claw_mptt_theme_setup9() {
		if (tiger_claw_exists_mptt()) {
			add_action( 'wp_enqueue_scripts', 						'tiger_claw_mptt_frontend_scripts', 1100 );
			add_filter( 'tiger_claw_filter_merge_styles',				'tiger_claw_mptt_merge_styles' );
		}
		if (is_admin()) {
			add_filter( 'tiger_claw_filter_tgmpa_required_plugins',	'tiger_claw_mptt_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'tiger_claw_mptt_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('tiger_claw_filter_tgmpa_required_plugins',	'tiger_claw_mptt_tgmpa_required_plugins');
	function tiger_claw_mptt_tgmpa_required_plugins($list=array()) {
		if (in_array('mp-timetable', tiger_claw_storage_get('required_plugins'))) {
			$list[] = array(
					'name' 		=> esc_html__('MP Timetable', 'tiger-claw'),
					'slug' 		=> 'mp-timetable',
					'required' 	=> false
			);
		}
		return $list;
	}
}

// Check if plugin is installed and activated
if ( !function_exists( 'tiger_claw_exists_mptt' ) ) {
	function tiger_claw_exists_mptt() {
		return class_exists('Mp_Time_Table');
	}
}
	
// Enqueue plugin's custom styles
if ( !function_exists( 'tiger_claw_mptt_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'tiger_claw_mptt_frontend_scripts', 1100 );
	function tiger_claw_mptt_frontend_scripts() {
		if (tiger_claw_exists_mptt()) {
			if (tiger_claw_is_on(tiger_claw_get_theme_option('debug_mode')) && tiger_claw_get_file_dir('plugins/mp-timetable/mp-timetable.css')!='')
				wp_enqueue_style( 'mp-timetable',  tiger_claw_get_file_url('plugins/mp-timetable/mp-timetable.css'), array(), null );
		}
	}
}
	
// Merge custom styles
if ( !function_exists( 'tiger_claw_mptt_merge_styles' ) ) {
	function tiger_claw_mptt_merge_styles($list) {
		$list[] = 'plugins/mp-timetable/mp-timetable.css';
		return $list;
	}
}


// Add plugin-specific colors and fonts to the custom CSS
if (tiger_claw_exists_mptt()) { require_once TIGER_CLAW_THEME_DIR . 'plugins/mp-timetable/mp-timetable.styles.php'; }
?>