<?php
/* WP GDPR Compliance support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if ( ! function_exists( 'tiger_claw_wp_gdpr_compliance_theme_setup9' ) ) {
	add_action( 'after_setup_theme', 'tiger_claw_wp_gdpr_compliance_theme_setup9', 9 );
	function tiger_claw_wp_gdpr_compliance_theme_setup9() {
		if ( is_admin() ) {
			add_filter( 'tiger_claw_filter_tgmpa_required_plugins', 'tiger_claw_wp_gdpr_compliance_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'tiger_claw_wp_gdpr_compliance_tgmpa_required_plugins' ) ) {
	function tiger_claw_wp_gdpr_compliance_tgmpa_required_plugins($list=array()) {
		if (in_array('wp-gdpr-compliance', tiger_claw_storage_get('required_plugins')))
			$list[] = array(
				'name' 		=> esc_html__('WP GDPR Compliance', 'tiger-claw'),
				'slug' 		=> 'wp-gdpr-compliance',
				'required' 	=> false
			);
		return $list;
	}
}


// Check if this plugin installed and activated
if ( ! function_exists( 'tiger_claw_exists_wp_gdpr_compliance' ) ) {
	function tiger_claw_exists_wp_gdpr_compliance() {
		return class_exists( 'WPGDPRC\WPGDPRC' );
	}
}
