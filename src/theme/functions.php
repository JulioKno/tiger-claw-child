<?php 

/**
 * Child-Theme functions and definitions
 */

function tiger_claw_child_scripts() {
    wp_enqueue_script( 'tiger-claw-child-script', get_stylesheet_directory_uri(). '/js/footer-bundle.js', array('jquery'), null, 1.0 );
}
add_action( 'wp_enqueue_scripts', 'tiger_claw_child_scripts' );


define ('WPCF7_AUTOP', false );

//Change post type visibility
function jka_hide_cpt_visibility( $args, $post_type ) {
    if ( 'cpt_team' == $post_type ) {
        $args['publicly_queryable'] = false;
        $args['has_archive'] = false;
   }
   return $args;
}
add_filter( 'register_post_type_args', 'jka_hide_cpt_visibility', 10, 2 );

// Prevent login throug wp-admin / wp-login
function jka_prevent_wp_login() {
    if ( class_exists( 'WooCommerce' ) ) {
        // WP tracks the current page - global the variable to access it
        global $pagenow;
        // Check if a $_GET['action'] is set, and if so, load it into $action variable
        $action = (isset($_GET['action'])) ? $_GET['action'] : '';
        // Check if we're on the login page, and ensure the action is not 'logout'
        if( $pagenow == 'wp-login.php' && ( ! $action || ( $action && ! in_array($action, array('logout', 'lostpassword', 'rp', 'resetpass'))))) {
            // Load the home page url
            $page = get_permalink( wc_get_page_id( 'myaccount' ) );
            // Redirect to the home page
            wp_redirect($page);
            // Stop execution to prevent the page loading for any reason
            exit();
        }
    }
}
add_action('init', 'jka_prevent_wp_login');


// Add shop manager css to hide things
function jka_shop_manager_enqueue_scripts() {
    $user = wp_get_current_user();
    if ( in_array( 'shop_manager', (array) $user->roles ) ) {
        wp_enqueue_style( 'shop-manager', get_stylesheet_directory_uri().'/admin/shop-manager.css', array(), 0.1);
    }
}
add_action( 'admin_enqueue_scripts', 'jka_shop_manager_enqueue_scripts' );

function jka_dashboard_redirect(){
    $user = wp_get_current_user();
    if ( in_array( 'shop_manager', (array) $user->roles ) ) {
        wp_redirect(admin_url('edit.php?post_type=tribe_events'));
    }
}
add_action('load-index.php','jka_dashboard_redirect');

function jka_login_redirect( $redirect_to, $request, $user ){
    $user = wp_get_current_user();
    if ( in_array( 'shop_manager', (array) $user->roles ) ) {
        return admin_url('edit.php?post_type=tribe_events');
    }
}
add_filter('login_redirect','jka_login_redirect',10,3);

function jka_remove_menus() {
    $user = wp_get_current_user();
    if ( in_array( 'shop_manager', (array) $user->roles ) ) {
        global $menu;
        $restricted = array(__('Dashboard'));
        //$restricted = array(__('Dashboard'), __('Posts'), __('Media'), __('Links'), __('Pages'), __('Appearance'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'));
        end($menu);
        while(prev($menu)){
            $value = explode(' ',$menu[key($menu)][0]);
            if(in_array($value[0]!= NULL?$value[0]:'',$restricted)){unset($menu[key($menu)]);}
        }
    }
}
add_action('admin_menu','jka_remove_menus');

//Rename menu elements. "Woocommerce" to "Pagos"
function jka_wc_admin_rename_labels() {
    global $menu;
   
    $searchPlugin = "woocommerce";
    $replaceName = "Pagos";
    $replaceIcon = "dashicons-products";

    $menuItem = "";
    foreach($menu as $key => $item){
        if ( $item[2] === $searchPlugin ){
            $menuItem = $key;
        }
    }
    if($menuItem){
        $menu[$menuItem][0] = $replaceName;
        $menu[$menuItem][6] = $replaceIcon;
    }
}
add_action( 'admin_menu', 'jka_wc_admin_rename_labels' );

//Replace woocommerce icon for dashicon "dashicons-products"
function jka_wc_admin_custom_icon() {
  echo '<style>
    #adminmenu #toplevel_page_woocommerce .menu-icon-generic div.wp-menu-image::before {
        font-family: dashicons !important;
        content: "\f312" !important;
    }
  </style>';
}
add_action('admin_head', 'jka_wc_admin_custom_icon');

//Remove links
function jka_remove_links_scripts(){
    $user = wp_get_current_user();
    if ( in_array( 'shop_manager', (array) $user->roles ) ) {
    ?>
        <script>
            (function($){
                var removeLinks = [
                    'tr.iedit .column-title a.row-title',
                    '#tribe-order-summary .welcome-panel-first li.venue-name a',
                    '#tribe-attendees-summary .welcome-panel-first li.venue-name a',
                ];
                $.each(removeLinks,function(index, link){
                    var $link = $(link);

                    $link.each(function(index, element){
                        var $this = $(element);
                        var $linkParent = $this.parent();
                        var linkText = $this.text();
                        $this.remove();
                        $linkParent.append(linkText);
                    });
                });

            })(jQuery)
        </script>
    <?php
    }
}
add_action('admin_footer', 'jka_remove_links_scripts');

/**
 * Agregar cabecero formas de pago
 */

// define the woocommerce_review_order_before_payment callback 
function jka_woocommerce_review_order_before_payment( ) { 
    echo "<h3>Seleccionar forma de pago</h3>";
}
        
// add the action 
add_action( 'woocommerce_review_order_before_payment', 'jka_woocommerce_review_order_before_payment', 10, 0 ); 