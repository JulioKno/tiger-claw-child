(function($){
	var $menuMobile = $('.menu_mobile');
	var $navBar = $('.main-menu-container-row');
	$(window).load(function() {
	    if (location.hash) {
			if ($(location.hash).length){
			  setTimeout(function() {
			    scrollToTarget(location.hash);
			  }, 100);
			};
		}
	});

	$('.menu-item a').click(function(e) {
		var hash = this.hash;
		e.preventDefault();
		if ($(hash).length){
			if($menuMobile.hasClass('opened')) $menuMobile.removeClass('opened');
			scrollToTarget(hash);
		}else{
			window.location.href = $(this).attr('href');
		}
	});

	$('.sc_layouts_menu_nav > li ul li.current-menu-item').removeClass('current-menu-item');

	function scrollToTarget(target){
		var navHeight = $navBar.length ? $navBar.outerHeight() : 50;
		$('html, body').animate({
			scrollTop: $(target).offset().top - navHeight
		}, 1000);
	}

	// Poner divisor menú mobile
	$('#menu_mobile-jka-menu-mobile li:last').prev().before('<li><hr style="margin: 1em 0 !important; border-color: #f5f5f5 !important;" /></li>');

	// Arreglar actualización de carrito al cambiar cantidad
	$(document).on('mouseup', 'div.quantity *', function(){
		var $this = $(this);
		if($this.hasClass('q_inc') || $this.hasClass('q_dec')) $this.siblings('input.qty').trigger('change');	
	});

})(jQuery);