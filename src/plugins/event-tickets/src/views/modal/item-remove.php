<?php
/**
 * Modal: Remove Item
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe/tickets/modal/item-remove.php
 *
 * @since 4.11.0
 * @since TBD Reformat a bit of the code around the button - no functional changes.
 *
 * @version TBD
 *
 */
?>
<div
	class="tribe-tickets__item__remove__wrap"
>
	<button
		type="button"
		class="tribe-tickets__item__remove"
	>
	</button>
</div>
